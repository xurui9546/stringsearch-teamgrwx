package com.eaio.stringsearch.statementCoverage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.eaio.stringsearch.BNDMWildcardsCI;
import com.eaio.stringsearch.StringSearch;

public class WrapperFuncTestForBNDMWildcardsCI {
	private static int NOT_FOUND = -1;
	private StringSearch ssBNDMWildcardsCI;
	private int loc;
	
	@Before
	public void init() {
		ssBNDMWildcardsCI = new BNDMWildcardsCI('.');
		loc = Integer.MIN_VALUE;
	}

	/* This test is used to cover all wrapper function of class BNDMWildcardsCI
	 * 
	 */
	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_tE_ptn_ForStr(){
		String txt = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcardsCI.processString(ptn);
		 
		loc = ssBNDMWildcardsCI.searchString(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_ptn_pObj_ForStr(){
		String txt = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcardsCI.processString(ptn);
		 
		loc = ssBNDMWildcardsCI.searchString(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_ptn_ForStr(){
		String txt = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcardsCI.processString(ptn);
		 
		loc = ssBNDMWildcardsCI.searchString(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_ptn_pObj_ForStr(){
		String txt = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcardsCI.processString(ptn);
		 
		loc = ssBNDMWildcardsCI.searchString(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBNDMWildcardsCI_txt_ptn_ForStr(){
		String txt = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcardsCI.processString(ptn);
		 
		loc = ssBNDMWildcardsCI.searchString(txt, ptn);
		assertTrue(loc == 3);
	}
	
	/////////////////////

	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_tE_ptn_ForChars(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		 
		loc = ssBNDMWildcardsCI.searchChars(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_ptn_pObj_ForChars(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMWildcardsCI.processChars(ptn);
		 
		loc = ssBNDMWildcardsCI.searchChars(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_ptn_ForChars(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMWildcardsCI.processChars(ptn);
		 
		loc = ssBNDMWildcardsCI.searchChars(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_ptn_pObj_ForChars(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMWildcardsCI.processChars(ptn);
		 
		loc = ssBNDMWildcardsCI.searchChars(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBNDMWildcardsCI_txt_ptn_ForChars(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMWildcardsCI.processChars(ptn);
		 
		loc = ssBNDMWildcardsCI.searchChars(txt, ptn);
		assertTrue(loc == 3);
	}
	
	///////////////

	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_tE_ptn_ForBytes(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		 
		loc = ssBNDMWildcardsCI.searchBytes(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_ptn_pObj_ForBytes(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMWildcardsCI.processBytes(ptn);
		 
		loc = ssBNDMWildcardsCI.searchBytes(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_tS_ptn_ForBytes(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMWildcardsCI.processBytes(ptn);
		 
		loc = ssBNDMWildcardsCI.searchBytes(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcardsCI_txt_ptn_pObj_ForBytes(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMWildcardsCI.processBytes(ptn);
		 
		loc = ssBNDMWildcardsCI.searchBytes(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBNDMWildcardsCI_txt_ptn_ForBytes(){
		String text = "!!!\t[031492] SU.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMWildcardsCI.processBytes(ptn);
		 
		loc = ssBNDMWildcardsCI.searchBytes(txt, ptn);
		assertTrue(loc == 3);
	}
}
