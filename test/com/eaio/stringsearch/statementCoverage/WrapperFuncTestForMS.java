package com.eaio.stringsearch.statementCoverage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.eaio.stringsearch.MismatchSearch;
import com.eaio.stringsearch.ShiftOrMismatches;

public class WrapperFuncTestForMS {
	private static int NOT_FOUND = -1;
	private static int NOT_MATCHED = 0;
	private static int FULL_MATCHED = 2;
	private MismatchSearch ssMS;
	private int[] locs = {Integer.MIN_VALUE, Integer.MIN_VALUE};
	
	@Before
	public void init() {
		ssMS = new ShiftOrMismatches();
		locs[0] = Integer.MIN_VALUE;
		locs[1] = Integer.MIN_VALUE;
	}

	/* This test is used to cover all wrapper function of class ShiftOrMismatch
	 * 
	 */
	@Test
	public void wft_ssMS_txt_tS_tE_ptn_prcd_k_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn, k);
		 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_tE_ptn_k_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn, k);
		 
		locs = ssMS.searchString(txt, tS, tE, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_prcd_k_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn, k);
		 
		locs = ssMS.searchString(txt, tS, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_k_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn, k);
		 
		locs = ssMS.searchString(txt, tS, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_tx_ptn_prcd_k_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn, k);
		 
		locs = ssMS.searchString(txt, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_ptn_k_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn, k);
		 
		locs = ssMS.searchString(txt, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	//
	
	@Test
	public void wft_ssMS_txt_tS_tE_ptn_prcd_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn);
		 
		locs[0] = ssMS.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_tE_ptn_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn);
		 
		locs[0] = ssMS.searchString(txt, tS, tE, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_prcd_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn);
		 
		locs[0] = ssMS.searchString(txt, tS, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn);
		 
		locs[0] = ssMS.searchString(txt, tS, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_tx_ptn_prcd_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn);
		 
		locs[0] = ssMS.searchString(txt, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_ptn_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processString(ptn);
		 
		locs[0] = ssMS.searchString(txt, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

	//////////////////
	@Test
	public void wft_ssMS_txt_tS_tE_ptn_prcd_k_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		 
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_tE_ptn_k_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		 
		locs = ssMS.searchChars(txt, tS, tE, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_prcd_k_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		 
		locs = ssMS.searchChars(txt, tS, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_k_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		 
		locs = ssMS.searchChars(txt, tS, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_tx_ptn_prcd_k_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		 
		locs = ssMS.searchChars(txt, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_ptn_k_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		 
		locs = ssMS.searchChars(txt, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}
	
	//

	@Test
	public void wft_ssMS_txt_tS_tE_ptn_prcd_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn);
		 
		locs[0] = ssMS.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_tE_ptn_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn);
		 
		locs[0] = ssMS.searchChars(txt, tS, tE, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_prcd_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn);
		 
		locs[0] = ssMS.searchChars(txt, tS, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn);
		 
		locs[0] = ssMS.searchChars(txt, tS, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_tx_ptn_prcd_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn);
		 
		locs[0] = ssMS.searchChars(txt, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_ptn_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn);
		 
		locs[0] = ssMS.searchChars(txt, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

	//////////////////////
	@Test
	public void wft_ssMS_txt_tS_tE_ptn_prcd_k_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		 
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_tE_ptn_k_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		 
		locs = ssMS.searchBytes(txt, tS, tE, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_prcd_k_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		 
		locs = ssMS.searchBytes(txt, tS, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_k_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		 
		locs = ssMS.searchBytes(txt, tS, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_tx_ptn_prcd_k_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		 
		locs = ssMS.searchBytes(txt, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

	@Test
	public void wft_ssMS_txt_ptn_k_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		 
		locs = ssMS.searchBytes(txt, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}
	
	//

	@Test
	public void wft_ssMS_txt_tS_tE_ptn_prcd_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn);
		 
		locs[0] = ssMS.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_tE_ptn_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn);
		 
		locs[0] = ssMS.searchBytes(txt, tS, tE, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_prcd_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn);
		 
		locs[0] = ssMS.searchBytes(txt, tS, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_tS_ptn_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn);
		 
		locs[0] = ssMS.searchBytes(txt, tS, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_tx_ptn_prcd_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn);
		 
		locs[0] = ssMS.searchBytes(txt, ptn, pObj);
		assertTrue(locs[0] == NOT_FOUND);
	}

	@Test
	public void wft_ssMS_txt_ptn_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn);
		 
		locs[0] = ssMS.searchBytes(txt, ptn);
		assertTrue(locs[0] == NOT_FOUND);
	}

}
