package com.eaio.stringsearch.statementCoverage;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

import com.eaio.stringsearch.CharIntMap;

public class CharIntMapTest {
	@Test
	public void coverdefaultConstructorOfCharIntMap() {
  		// Technique used: statement coverage
  		//
		// cover line 71-72
		//
  		// No one call default constructor of CharIntMap
		boolean result = false;
		CharIntMap charintMap = new CharIntMap();
		if(charintMap != null) {
			result = true;
		}
		assertEquals(true,result);
	}
	
	@Test
	public void coversetAndgetLowestOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 116, 135-136
		//
  		// if this branch can be reached, the assignment will fail. The program
		// prevent this happening.
		// getLowest is never called.
		boolean result = false;
		CharIntMap charintMap = new CharIntMap(1,'c',0);
		charintMap.set('x', '1');
		int z = 'x' - 'c';
		if( ('x'- charintMap.getLowest() )> 1) {
			result = true;
		}
		assertEquals(true,result);
	}
	
	@Test
	public void covergetExentOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 127
  		// getExtent method is never called.
		CharIntMap charintMap = new CharIntMap(1,'c',0);
		int extent = charintMap.getExtent();
		assertEquals(extent,1);
	}
	
	@Test
	public void covergetHighestOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 145
  		// getHighest method is never called.
		CharIntMap charintMap = new CharIntMap(10,'c',0);
		char c = charintMap.getHighest();
		assertEquals(c,'m');
	}
	
	@Test
	public void coverequalsOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 157-173
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap(10,'c',0);
		CharIntMap charintMap2 = new CharIntMap(10,'c',0);
		boolean result = false;
		if(charintMap1.equals(charintMap2)) result = true;
		assertEquals(result,true);
	}
	
	@Test
	public void coverDifferentDefaultValueOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 168
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap(10,'c',0);
		CharIntMap charintMap2 = new CharIntMap(10,'c',1);
		boolean result = false;
		if(!charintMap1.equals(charintMap2)) result = true;
		assertEquals(result,true);
	}
	@Test
	public void coverSameObjectOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 157-158
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap(10,'c',0);
		boolean result = false;
		if(charintMap1.equals(charintMap1)) result = true;
		assertEquals(result,true);
	}
	
	@Test
	public void coverNotInstanceOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 160-161
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap(10,'c',0);
		String temp = new String();
		boolean result = true;
		if(!charintMap1.equals(temp)) result = false;
		assertEquals(result,false);
	}
	
	@Test
	public void coverhaveDifferentLowestOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 168
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap(10,'c',0);
		CharIntMap charintMap2 = new CharIntMap(10,'a',0);
		boolean result = true;
		if(!charintMap1.equals(charintMap2)) result = false;
		assertEquals(result,false);
	}
	
	@Test
	public void coverarraysAreNullOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 171
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap();
		CharIntMap charintMap2 = new CharIntMap();
		boolean result = false;
		if(charintMap1.equals(charintMap2)) result = true;
		assertEquals(result,true);
	}
	@Test
	public void coverFirstIsNullOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 171
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap();
		CharIntMap charintMap2 = new CharIntMap(10,'c',10);
		boolean result = false;
		if(!charintMap1.equals(charintMap2)) result = true;
		assertEquals(result,true);
	}
	@Test
	public void coverSecondIsNullOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 171
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap(10,'c',10);
		CharIntMap charintMap2 = new CharIntMap();
		boolean result = false;
		if(!charintMap1.equals(charintMap2)) result = true;
		assertEquals(result,true);
	}
	@Test
	public void coverNoneIsNullOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 171
  		// equal method is never called.
		CharIntMap charintMap1 = new CharIntMap(10,'c',10);
		CharIntMap charintMap2 = new CharIntMap(1,'d',10);
		boolean result = false;
		if(!charintMap1.equals(charintMap2)) result = true;
		assertEquals(result,true);
	}
	
	@Test
	public void coverhashCodeOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 184 - 192
  		// equal method is never called.
		CharIntMap charintMap = new CharIntMap(10,'c',0);
		int x = charintMap.hashCode();
		assertEquals(x,1974383706);
	}
	@Test
	public void coverhashCodeArrayNullOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 184 - 192
  		// equal method is never called.
		CharIntMap charintMap = new CharIntMap();
		int x = charintMap.hashCode();
		assertEquals(x,1974383673);
	}
	@Test
	public void covertoStringOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 204
  		// equal method is never called.
		CharIntMap charintMap = new CharIntMap(10,'c',0);
		String resultStr = charintMap.toString();
		String expected = "{ CharIntMap: lowest = c, defaultValue = 0, array = }";;
		boolean result = false;
		if(resultStr.equals(expected)){
			result = true;
		}
		assertEquals(result,true);
	}
	
	@Test
	public void covertoStringBufferOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 204
  		// equal method is never called.
		CharIntMap charintMap = new CharIntMap(10,'c',0);
		charintMap.set('d', 1);
		StringBuffer sb = new StringBuffer();
		charintMap.toStringBuffer(sb);
		String expected = "{ CharIntMap: lowest = c, defaultValue = 0, array = 1: 1 }";
		boolean result = false;
		if(sb.toString().equals(expected)){
			result = true;
		}
		assertEquals(result,true);
	}
	
	@Test
	public void covertoStringBufferArrayNullOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 204
  		// equal method is never called.
		CharIntMap charintMap = new CharIntMap();
		StringBuffer sb = new StringBuffer();
		charintMap.toStringBuffer(sb);
		String expected = "{ CharIntMap: lowest = " + charintMap.getLowest()+", defaultValue = 0}";
		String str = sb.toString();
		boolean result = false;
		if(str.equals(expected)) result = true;
		assertEquals(result,true);
	}
	
	@Test
	public void coverreadAndWirteObjectOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 204
  		// equal method is never called.
		File file = new File("store.out");
		CharIntMap charintMap1 = new CharIntMap(10,'c',0);
		charintMap1.set('d', 1);
		charintMap1.set('e', 2);
		charintMap1.set('f', 3);
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
			charintMap1.writeExternal(oos);
			oos.close();
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
			CharIntMap charintMap2 = new CharIntMap();
			charintMap2.readExternal(ois);
			boolean result = false;
			if(charintMap1.equals(charintMap2)) {
				result = true;
			}
			assertEquals(result,true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Test
	public void coverreadAndWirteNullObjectOfCharIntMap() {
  		// Technique used: statement coverage
  		// cover line 204
  		// equal method is never called.
		File file = new File("store.out");
		CharIntMap charintMap1 = new CharIntMap();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
			charintMap1.writeExternal(oos);
			oos.close();
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
			CharIntMap charintMap2 = new CharIntMap();
			charintMap2.readExternal(ois);
			boolean result = false;
			if(charintMap1.equals(charintMap2)) {
				result = true;
			}
			assertEquals(result,true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
