package com.eaio.stringsearch.statementCoverage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.eaio.stringsearch.BNDM;
import com.eaio.stringsearch.StringSearch;

public class WrapperFuncTestForBNDM {
	private static int NOT_FOUND = -1;
	private StringSearch ssBNDM;
	private int loc;
	
	@Before
	public void init() {
		ssBNDM = new BNDM();
		loc = Integer.MIN_VALUE;
	}

	/* This test is used to cover all wrapper function of class BNDM
	 * 
	 */
	@Test
	public void wft_ssBNDM_txt_tS_tE_ptn_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDM.processString(ptn);
		 
		loc = ssBNDM.searchString(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_tS_ptn_pObj_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDM.processString(ptn);
		 
		loc = ssBNDM.searchString(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_tS_ptn_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDM.processString(ptn);
		 
		loc = ssBNDM.searchString(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_ptn_pObj_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDM.processString(ptn);
		 
		loc = ssBNDM.searchString(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBNDM_txt_ptn_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDM.processString(ptn);
		 
		loc = ssBNDM.searchString(txt, ptn);
		assertTrue(loc == 3);
	}
	
	/////////////////////

	@Test
	public void wft_ssBNDM_txt_tS_tE_ptn_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		 
		loc = ssBNDM.searchChars(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_tS_ptn_pObj_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDM.processChars(ptn);
		 
		loc = ssBNDM.searchChars(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_tS_ptn_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDM.processChars(ptn);
		 
		loc = ssBNDM.searchChars(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_ptn_pObj_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDM.processChars(ptn);
		 
		loc = ssBNDM.searchChars(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBNDM_txt_ptn_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDM.processChars(ptn);
		 
		loc = ssBNDM.searchChars(txt, ptn);
		assertTrue(loc == 3);
	}
	
	///////////////

	@Test
	public void wft_ssBNDM_txt_tS_tE_ptn_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		 
		loc = ssBNDM.searchBytes(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_tS_ptn_pObj_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDM.processBytes(ptn);
		 
		loc = ssBNDM.searchBytes(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_tS_ptn_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDM.processBytes(ptn);
		 
		loc = ssBNDM.searchBytes(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_ptn_pObj_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDM.processBytes(ptn);
		 
		loc = ssBNDM.searchBytes(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDM_txt_ptn_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDM.processBytes(ptn);
		 
		loc = ssBNDM.searchBytes(txt, ptn);
		assertTrue(loc == 3);
	}
}
