package com.eaio.stringsearch.statementCoverage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.eaio.stringsearch.BNDMWildcards;
import com.eaio.stringsearch.StringSearch;

public class WrapperFuncTestForBNDMWildcards {
	private static int NOT_FOUND = -1;
	private StringSearch ssBNDMWildcards;
	private int loc;
	
	@Before
	public void init() {
		ssBNDMWildcards = new BNDMWildcards();
		loc = Integer.MIN_VALUE;
	}

	/* This test is used to cover all wrapper function of class BNDMWildcards
	 * 
	 */
	@Test
	public void wft_ssBNDMWildcards_txt_tS_tE_ptn_ForStr(){
		String txt = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcards.processString(ptn);
		 
		loc = ssBNDMWildcards.searchString(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_tS_ptn_pObj_ForStr(){
		String txt = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcards.processString(ptn);
		 
		loc = ssBNDMWildcards.searchString(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_tS_ptn_ForStr(){
		String txt = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcards.processString(ptn);
		 
		loc = ssBNDMWildcards.searchString(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_ptn_pObj_ForStr(){
		String txt = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcards.processString(ptn);
		 
		loc = ssBNDMWildcards.searchString(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBNDMWildcards_txt_ptn_ForStr(){
		String txt = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMWildcards.processString(ptn);
		 
		loc = ssBNDMWildcards.searchString(txt, ptn);
		assertTrue(loc == 3);
	}
	
	/////////////////////

	@Test
	public void wft_ssBNDMWildcards_txt_tS_tE_ptn_ForChars(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		 
		loc = ssBNDMWildcards.searchChars(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_tS_ptn_pObj_ForChars(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMWildcards.processChars(ptn);
		 
		loc = ssBNDMWildcards.searchChars(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_tS_ptn_ForChars(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMWildcards.processChars(ptn);
		 
		loc = ssBNDMWildcards.searchChars(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_ptn_pObj_ForChars(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMWildcards.processChars(ptn);
		 
		loc = ssBNDMWildcards.searchChars(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBNDMWildcards_txt_ptn_ForChars(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMWildcards.processChars(ptn);
		 
		loc = ssBNDMWildcards.searchChars(txt, ptn);
		assertTrue(loc == 3);
	}
	
	///////////////

	@Test
	public void wft_ssBNDMWildcards_txt_tS_tE_ptn_ForBytes(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		 
		loc = ssBNDMWildcards.searchBytes(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_tS_ptn_pObj_ForBytes(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMWildcards.processBytes(ptn);
		 
		loc = ssBNDMWildcards.searchBytes(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_tS_ptn_ForBytes(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMWildcards.processBytes(ptn);
		 
		loc = ssBNDMWildcards.searchBytes(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBNDMWildcards_txt_ptn_pObj_ForBytes(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMWildcards.processBytes(ptn);
		 
		loc = ssBNDMWildcards.searchBytes(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBNDMWildcards_txt_ptn_ForBytes(){
		String text = "!!!\t[031492] Su.Lin.Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMWildcards.processBytes(ptn);
		 
		loc = ssBNDMWildcards.searchBytes(txt, ptn);
		assertTrue(loc == 3);
	}
}
