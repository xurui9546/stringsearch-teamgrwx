package com.eaio.stringsearch.statementCoverage;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.eaio.stringsearch.MismatchSearch;
import com.eaio.stringsearch.ShiftOrMismatches;

public class MismatchCoverageTest {

	//Technique used: statement coverage
	
	//the following 6 test cases are based on the method searchBytes()
	//cover line 139, it's an outer method which will call another method realized in subclass, with a default input of k=0
	@Test
	 public void testMismatchsearchBytesWith5InputsWithoutK(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int[] a = new int[] {1, 0};
			int textStart = 0;
			int textEnd = text.length();
			int locations = ss.searchBytes(text.getBytes(), textStart, textEnd, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
			assertEquals(1, locations);
	 }
	
	//cover line 158, it's an outer method with 3 parameters as inputs, it will also call a six parameters input method with 
	//textstart, textend being default values and pre-processed object corresponding to pattern
	@Test
	public void testMismatchsearchBytesWith3Inputs(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int []a = new int[] {0, 2};
			int k=pattern.length()-1;
			int []locations = ss.searchBytes(text.getBytes(), pattern.getBytes(),k);
	
			assertArrayEquals(a, locations);
		}
	
	//cover line 198, with an additional input of textStart comparing to the method of 3 inputs
	@Test
	public void testMismatchsearchBytesWith4InputsTextStart(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int []a = new int[] {0, 2};
			int textStart = 0;
			int k=pattern.length()-1;
			int []locations = ss.searchBytes(text.getBytes(), textStart, pattern.getBytes(),k);
	
			assertArrayEquals(a, locations);
		}
	
	
	//cover line 177,with an additional input of pre-processed object comparing to the method of 3 inputs
	@Test
	public void testMismatchsearchBytesWith4InputsPreprocessedObject(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int []a = new int[] {0, 2};
			int textStart = 0;
			int k=pattern.length()-1;
			Object aa = ss.processBytes(pattern.getBytes(),k);
			int []locations = ss.searchBytes(text.getBytes(),pattern.getBytes(),aa,k);
			
			
			assertArrayEquals(a, locations);
		}
	//cover line 220, inputs are 5 parameters without textEnd
	@Test
	public void testMismatchsearchBytesWith5InputsTextStart(){
		MismatchSearch ss = new ShiftOrMismatches();
		String text = "hellohello";
		String pattern = "ell";
		int []a = new int[] {0, 2};
		int textStart = 0;
		int k=pattern.length()-1;
		int []locations = ss.searchBytes(text.getBytes(), textStart, pattern.getBytes(),ss.processBytes(pattern.getBytes(),k),k);

		assertArrayEquals(a, locations);
	}
	
	//cover line 242, inputs are 5 parameters with out pre-processed objects
	@Test
	public void testMismatchsearchBytesWith5InputsWithoutObject(){
		MismatchSearch ss = new ShiftOrMismatches();
		String text = "hellohello";
		String pattern = "ell";
		int []a = new int[] {0, 2};
		int textStart = 0;
		int textEnd = pattern.length();
		int k=pattern.length()-1;
		int []locations = ss.searchBytes(text.getBytes(), textStart, textEnd, pattern.getBytes(),k);

		assertArrayEquals(a, locations);
	}
	
	//the following 5 testcases are based on searchChars()
	//cover line 281, the inputs are 5 without max mismatch number k
	
	@Test
	 public void testMismatchsearchCharsWith5InputsWithoutK(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int[] a = new int[] {1, 0};
			int textStart = 0;
			int textEnd = text.length();
			int locations = ss.searchChars(text.toCharArray(), textStart, textEnd, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
			assertEquals(1, locations);
	 }
	
	@Test
	//cover line 297, there are 3 inputs, text, pattern, k
	 public void testMismatchsearchCharsWith3Inputs(){
		MismatchSearch ss = new ShiftOrMismatches();
		String text = "hellohello";
		String pattern = "ell";
		int []a = new int[] {0, 2};
		int k=pattern.length()-1;
		int []locations = ss.searchChars(text.toCharArray(), pattern.toCharArray(),k);

		assertArrayEquals(a, locations);
	}
	
	@Test
	//cover line 314, 4 inputs with textstart comparing to 3 input one 
	public void testMismatchsearchCharsWith4InputsTextStart(){
		MismatchSearch ss = new ShiftOrMismatches();
		String text = "hellohello";
		String pattern = "ell";
		int []a = new int[] {0, 2};
		int textStart = 0;
		int k=pattern.length()-1;
		int []locations = ss.searchChars(text.toCharArray(), textStart, pattern.toCharArray(),k);

		assertArrayEquals(a, locations);
	}
	
	//cover line 333, 5 inputs with textstart
	@Test
	public void testMismatchsearchCharsWith5InputsTextStart(){
		MismatchSearch ss = new ShiftOrMismatches();
		String text = "hellohello";
		String pattern = "ell";
		int []a = new int[] {0, 2};
		int textStart = 0;
		int k=pattern.length()-1;
		int []locations = ss.searchChars(text.toCharArray(), textStart, pattern.toCharArray(),ss.processChars(pattern.toCharArray(),k),k);

		assertArrayEquals(a, locations);
	}
	
	//cover line 317, 4 inputs with pre-processed object
	@Test
	public void testMismatchsearchCharsWith4InputsPreprocessedObject(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int []a = new int[] {0, 2};
			int k=pattern.length()-1;
			Object aa = ss.processChars(pattern.toCharArray(),k);
			int []locations = ss.searchChars(text.toCharArray(),pattern.toCharArray(),aa,k);
			
			
			assertArrayEquals(a, locations);
		}
	
	//the following 6 test cases are based on the method searchString()
	//cover line 359, the inputs are 5 without max mismatch number k
	@Test
	 public void testMismatchsearchStringWith5InputsWithoutK(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int[] a = new int[] {1, 0};
			int textStart = 0;
			int textEnd = text.length();
			int locations = ss.searchString(text, textStart, textEnd, pattern, ss.processString(pattern));
			assertEquals(1, locations);
	 }
	
	//cover line 376, there are 3 inputs, text, pattern, k
	@Test
	public void testMismatchsearchStringWith3Inputs(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int []a = new int[] {0, 2};
			int k=pattern.length()-1;
			int []locations = ss.searchString(text, pattern,k);
	
			assertArrayEquals(a, locations);
		}
	
	//cover line 416, with an additional input of textStart comparing to the method of 3 inputs
	@Test
	public void testMismatchsearchStringWith4InputsTextStart(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int []a = new int[] {0, 2};
			int textStart = 0;
			int k=pattern.length()-1;
			int []locations = ss.searchString(text, textStart, pattern,k);
	
			assertArrayEquals(a, locations);
		}
	
	//cover line 525, 5 inputs with textstart
	@Test
	public void testMismatchsearchStringWith5InputsTextStart(){
		MismatchSearch ss = new ShiftOrMismatches();
		String text = "hellohello";
		String pattern = "ell";
		int []a = new int[] {0, 2};
		int textStart = 0;
		int k=pattern.length()-1;
		int []locations = ss.searchString(text, textStart, pattern,ss.processString(pattern,k),k);

		assertArrayEquals(a, locations);
	}
	
	//cover line 500,5 inputs without pre-processed object
	@Test
	public void testMismatchsearchStringWith5InputsWithoutObject(){
		MismatchSearch ss = new ShiftOrMismatches();
		String text = "hellohello";
		String pattern = "ell";
		int []a = new int[] {0, 2};
		int textStart = 0;
		int textEnd = pattern.length();
		int k=pattern.length()-1;
		int []locations = ss.searchString(text, textStart, textEnd, pattern,k);

		assertArrayEquals(a, locations);
	}
	
	//cover line, 4 inputs with pre-processed object
	@Test
	public void testMismatchsearchStringWith4InputsPreprocessedObject(){
			MismatchSearch ss = new ShiftOrMismatches();
			String text = "hellohello";
			String pattern = "ell";
			int []a = new int[] {0, 2};
			int textStart = 0;
			int k=pattern.length()-1;
			int []locations = ss.searchString(text,pattern,ss.processString(pattern,k),k);
			
			
			assertArrayEquals(a, locations);
		}
}
