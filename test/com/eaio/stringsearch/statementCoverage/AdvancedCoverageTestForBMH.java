package com.eaio.stringsearch.statementCoverage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.eaio.stringsearch.BoyerMooreHorspool;
import com.eaio.stringsearch.BoyerMooreHorspoolRaita;
import com.eaio.stringsearch.StringSearch;

public class AdvancedCoverageTestForBMH {
	private static int NOT_FOUND = -1;
	private StringSearch ssBMH;
	private int loc;
	
	@Before
	public void init() {
		ssBMH = new BoyerMooreHorspool();
		loc = Integer.MIN_VALUE;
	}

	/*
	 * This suite is used to test BMH class with pattern having special lenghth 
	 */

	@Test
	public void act_ptnLen1_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "S";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 13);
	}
	
	@Test
	public void act_ptnLen2_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "Su";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 13);
	}

	@Test
	public void act_ptnLen1b_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 24;
		int tE = 24;
		String ptn = "S";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
	@Test
	public void act_ptnLenb_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 24;
		int tE = 24;
		String ptn = "Su";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	//////////////////

	@Test
	public void act_ptnLen1_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "S";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBMH.processBytes(ptn);

		loc = ssBMH.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 13);
	}
	
	@Test
	public void act_ptnLen2_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBMH.processBytes(ptn);

		loc = ssBMH.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 13);
	}

	@Test
	public void act_ptnLen1b_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 24;
		int tE = 24;
		String pattern = "S";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBMH.processBytes(ptn);

		loc = ssBMH.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
	@Test
	public void act_ptnLen2b_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 24;
		int tE = 24;
		String pattern = "Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBMH.processBytes(ptn);

		loc = ssBMH.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	//////////////////////
	@Test
	public void act_ptnLen1_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "S";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBMH.processChars(ptn);
		 
		loc = ssBMH.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 13);
	}


	@Test
	public void act_ptnLen2_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBMH.processChars(ptn);
		 
		loc = ssBMH.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 13);
	}

	@Test
	public void act_ptnLen1b_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 24;
		int tE = 24;
		String pattern = "S";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBMH.processChars(ptn);
		 
		loc = ssBMH.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}


	@Test
	public void act_ptnLen2b_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 24;
		int tE = 24;
		String pattern = "Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBMH.processChars(ptn);
		 
		loc = ssBMH.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
}
