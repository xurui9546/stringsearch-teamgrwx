package com.eaio.stringsearch.statementCoverage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.eaio.stringsearch.BoyerMooreHorspool;
import com.eaio.stringsearch.StringSearch;

public class WrapperFuncTestForBMH {
	private static int NOT_FOUND = -1;
	private StringSearch ssBMH;
	private int loc;
	
	@Before
	public void init() {
		ssBMH = new BoyerMooreHorspool();
		loc = Integer.MIN_VALUE;
	}

	/* This test is used to cover all wrapper function of class BoyerMooreHorspool
	 * 
	 */
	@Test
	public void wft_ssBMH_txt_tS_tE_ptn_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_tS_ptn_pObj_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_tS_ptn_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_ptn_pObj_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBMH_txt_ptn_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBMH.processString(ptn);
		 
		loc = ssBMH.searchString(txt, ptn);
		assertTrue(loc == 3);
	}
	
	/////////////////////

	@Test
	public void wft_ssBMH_txt_tS_tE_ptn_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		 
		loc = ssBMH.searchChars(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_tS_ptn_pObj_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBMH.processChars(ptn);
		 
		loc = ssBMH.searchChars(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_tS_ptn_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBMH.processChars(ptn);
		 
		loc = ssBMH.searchChars(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_ptn_pObj_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBMH.processChars(ptn);
		 
		loc = ssBMH.searchChars(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBMH_txt_ptn_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBMH.processChars(ptn);
		 
		loc = ssBMH.searchChars(txt, ptn);
		assertTrue(loc == 3);
	}
	
	///////////////

	@Test
	public void wft_ssBMH_txt_tS_tE_ptn_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		 
		loc = ssBMH.searchBytes(txt, tS, tE, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_tS_ptn_pObj_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBMH.processBytes(ptn);
		 
		loc = ssBMH.searchBytes(txt, tS, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_tS_ptn_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBMH.processBytes(ptn);
		 
		loc = ssBMH.searchBytes(txt, tS, ptn);
		assertTrue(loc == 3);
	}
	
	@Test
	public void wft_ssBMH_txt_ptn_pObj_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBMH.processBytes(ptn);
		 
		loc = ssBMH.searchBytes(txt, ptn, pObj);
		assertTrue(loc == 3);
	}
	@Test
	public void wft_ssBMH_txt_ptn_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBMH.processBytes(ptn);
		 
		loc = ssBMH.searchBytes(txt, ptn);
		assertTrue(loc == 3);
	}
}
