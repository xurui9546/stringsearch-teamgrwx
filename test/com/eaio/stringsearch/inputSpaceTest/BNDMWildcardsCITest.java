package com.eaio.stringsearch.inputSpaceTest;
import static org.junit.Assert.*;

import org.junit.Test;

import com.eaio.stringsearch.BNDMWildcardsCI;
import com.eaio.stringsearch.StringSearch;
public class BNDMWildcardsCITest {

  	@Test
  	public void hasNoIntersectionForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text and pattern have no intersection.
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwertyuio";
  		String pattern = "qoiue";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void hasIntersectionForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text and pattern have intersection
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "ertyui";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void textEqualToPatternForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text and pattern are equal
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "qwerty";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(0, location);
  	}
  	
  	@Test
  	public void textContainsPatternForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text contains pattern
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "rty";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(3, location);
  	}

  	@Test
  	public void patternContainsTestForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text contains pattern
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "rty";
  		String pattern = "qwerty";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
    //text and pattern are all lower case
  	//this case is same with above test cases
  	
  	@Test
  	public void testLowerTextAndLowerAndUpperPatternForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text is lower case and pattern contains upper case and lower case chars
  	  	StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "qwerty";
  	  	String pattern = "rTy";
  	  
  	  	int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}

  	@Test
  	public void testLowerAndUpperTextAndLowerPatternForString() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text contains upper case and lower case chars and pattern is lower case
  	  	StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "qwerTy";
  	  	String pattern = "rty";
  	  
  	  	int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}
  	
  	@Test
  	public void testUpperTextAndUpperPatternForString() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text and pattern are all upper case
  		StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "QWERTY";
  	  	String pattern = "RTY";
  	  
  	  	int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}
  	
  	//pattern doesn't contain wildcard
  	//this test case is same with above test cases

   	@Test
  	public void patternContainsWildcardsForString() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcards implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - pattern contains wildcard
  		StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "QWERTY";
  	  	String pattern = "R.Y";
  	  
  	  	int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}
  	
  	@Test
  	public void hasNoIntersectionForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text and pattern have no intersection
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwertyuio";
  		String pattern = "qoiue";
  		
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(-1, location);
  	}

  	@Test
  	public void hasIntersectionForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text and pattern have intersection
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "ertyui";
  		
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void textEqualToPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text and pattern are equal
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "qwerty";
  		
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(0, location);
  	}
  	
  	@Test
  	public void textContainsPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text contains pattern
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "rty";
  		
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(3, location);
  	}

  	@Test
  	public void patternContainsTestForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - pattern contains text
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "rty";
  		String pattern = "qwerty";
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(-1, location);
  	}
    //text and pattern are all lower case
  	//this case is same with above test cases
  	  
  	@Test
  	public void testLowerTextAndLowerAndUpperPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text is lower case and pattern contains upper case and lower case chars
  	  	StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "qwerty";
  	  	String pattern = "rTy";
  	  
  	  int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(3, location);
  	}
  	
  	@Test
  	public void testLowerAndUpperTextAndLowerPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text contains upper case and lower case chars and pattern is lower case
  	  	StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "qwerty";
  	  	String pattern = "rTy";
  	  
  	  int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(3, location);
  	}
  	
  	//text and pattern are all upper case
  	@Test
  	public void testUpperTextAndUpperPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text contains upper case and lower case chars and pattern is lower case
  		StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "QWERTY";
  	  	String pattern = "RTY";
  	  
  	  int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(3, location);
  	}
  	
  	//pattern doesn't contain wildcard
  	//this test case is same with above test cases

  	
  	@Test
  	public void patternContainsWildcardsForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - pattern contains wildcards.

  		StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "QWERTY";
  	  	String pattern = "R.Y";
  	  
  	  int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(3, location);
  	}
  	
  	@Test
  	public void hasNoIntersectionForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text and pattern have no intersection

  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwertyuio";
  		String pattern = "qoiue";
  		
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void hasIntersectionForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text and pattern have intersection
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "ertyui";
  		
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void textEqualToPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text and pattern are equal
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "qwerty";
  		
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(0, location);
  	}
  	

  	@Test
  	public void textContainsPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text contains pattern
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "qwerty";
  		String pattern = "rty";
  		
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(3, location);
  	}
  	
  	@Test
  	public void patternContainsTestForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text contains pattern
  		StringSearch ss = new BNDMWildcardsCI();
  		String str = "rty";
  		String pattern = "qwerty";
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
    //text and pattern are all lower case
  	//this case is same with above test cases
  	
  	@Test
  	public void testLowerTextAndLowerAndUpperPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text is lower case and pattern contains upper case and lower case chars
  	  	StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "qwerty";
  	  	String pattern = "rTy";
  	  
  	    int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(3, location);
  	}
  	
  	@Test
  	public void testLowerAndUpperTextAndLowerPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text contains upper case and lower case chars and pattern is lower case
  	  	StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "qwerTy";
  	  	String pattern = "rty";
  	  
  	    int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}
  	
  	@Test
  	public void testUpperTextAndUpperPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BNDMWildcardsCI implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text and pattern are all upper case
  		StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "QWERTY";
  	  	String pattern = "RTY";
  	  
  	    int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}
  	//pattern doesn't contain wildcard
  	//this test case is same with above test cases

  	//pattern contains wildcard
  	@Test
  	public void patternContainsWildcardsForChars() {
  		StringSearch ss = new BNDMWildcardsCI();
  	  	String str = "QWERTY";
  	  	String pattern = "R.Y";
  	  
  	  int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}
	//The following 3 test cases are based on the inputs to BNDMWildcardsCI
    //implementation of StringSearch.searchString that contains
    //this partition is about the location returned from the function searchString
    //there is 3 kinds of output -1, 0, >0
    
    //textstart and textend are both set to default value(start=0, end=text.length())
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCILocationIsNegativeOne(){
    	StringSearch ss = new BNDMWildcardsCI();
    	String text = "fault";
    	String pattern = "right";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 0 means can find pattern in text and the found position is at the beginning
    @Test
    public void testBNDMWildcardsCILocationIsZero(){
    	StringSearch ss = new BNDMWildcardsCI();
    	String text = "right tiger";
    	String pattern = "right";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(0, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 6 means can find pattern in text and the found position is 6 in text
    @Test
    public void testBNDMWildcardsCILocationIsPositive(){
    	StringSearch ss = new BNDMWildcardsCI();
    	String text = "right tiger";
    	String pattern = "tiger";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(6, location);
    }
	
    
    //The following 3 test cases are based on the inputs to BNDMWildcardsCI
    //implementation of StringSearch.searchString that contains
    //this partition is about the relation of the values of TextStart and TextEnd and pattern
    //there are 7 blocks of inputs
    //(TextStart<TextEnd)&(pattern is totally in this part), the final output should be the right position
    //(TextStart<TextEnd)&(pattern has some part included in this part), the final output should be -1
    //(TextStart<TextEnd)&(pattern has no part included in this part), the final output should be -1
    //(TextStart==TextEnd), whatever the pattern is, the final output should be the -1
    // TextStart>TextEnd, whatever the pattern is, it will always not in this part and return should be -1
    
   
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is 2 means can find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternIsTotallyInThisPart(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboi";
   		String pattern = "ip";
   		int textstart = 2;
   		int textend = 7;
   		Object aa = ss.processChars(pattern.toCharArray());
   		int location = ss.searchString(text, textstart, textend, pattern, aa);
   		assertEquals(2, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is partially in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternHasSomePartsIncludedInThisPart(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboi";
   		String pattern = "poib";
   		int textstart = 7;
   		int textend = 7;
   		Object aa = ss.processChars(pattern.toCharArray());
   		int location = ss.searchString(text, textstart, textend, pattern, aa);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern has no part in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternHasNoPartIncludedInThisPart(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboi";
   		String pattern = "boi";
   		int textstart = 2;
   		int textend = 5;
   		Object aa = ss.processChars(pattern.toCharArray());
   		int location = ss.searchString(text, textstart, textend, pattern, aa);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is -1 means can find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartEqualsTextEnd(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboipoipoiboi";
   		String pattern = "poipoiboi";
   		int textstart = 0;
   		int textend = 0;
   		Object aa = ss.processChars(pattern.toCharArray());
   		int location = ss.searchString(text, textstart, textend, pattern, aa);
   		System.out.println(location);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart larger than TextEnd
    //the location returned is -1 means can find pattern in text
    @Test
   	public void testBNDMWildcardsCITextStartLargerThanTextEnd(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "poipoiboi";
  		String pattern = "boi";
  		int textstart = 5;
  		int textend = 4;
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
  		
  }
    //the following test cases are based on the functionality aspect of the length
    //of text and pattern based on the inputs to BNDMWildcardsCI implementation of StringSearch.searchString
	//this partition is about the relation among the length of Text and pattern and 32 bytes
    
    //blocks are like:
    //Text<Pattern<32
    //Text<32<Pattern
    //32<Text<Pattern
    //Text=Pattern<32
    //Text=Pattern=32
    //Text=Pattern>32
    //Text>Pattern>32
    //Text>32>Pattern
    //32>Text>Pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart, TextEnd reasonable input
    //the location returned is -1 means can find pattern in text
    
    
    //Text<Pattern<32
    @Test
   	public void testBNDMWildcardsCITextSmallerThanPatternSmallerThan32(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "boi";
  		String pattern = "poipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
  		}
    //Text<32<Pattern
    @Test
   	public void testBNDMWildcardsCITextSmallerThan32SmallerThanPattern(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "boi";
  		String pattern = "poipoiboipoipoiboipoipoiboipoipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
  		}
    
    //32<Text<Pattern
    @Test
    public void testBNDMWildcardsCI32SmallerThanTextSmallerThanPattern(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "poipoiboipoipoiboipoipoiboipoipoiboiboi";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
  		
    }
    
    //Text=Pattern<32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternSmallerThan32(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboi";
  		String pattern = "poipoiboipoi";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
    }
    
    //Text=Pattern=32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternEqualsTo32(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboibo";
  		String pattern = "poipoiboipoipoipoiboipoipoipoibo";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
    }
    
    //Text=Pattern>32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternLargerThan32(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboiboiboiboiboiboiboipppppp";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(0,location);
    }
    
    //Text>Pattern>32
    //when pattern's length > 32, the processChars() will only deal with the first 32 bytes
    //in this string, so in this case, the output actually not the one it should be
    @Test
    public void testBNDMWildcardsCITextLargerThanPatternLargerThan32(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poiboiboiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboiboiboiboiboiboiboipppppp";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(3,location);
    }
    
    //Text>32>Pattern
    @Test
    public void testBNDMWildcardsCITextLargerThan32LargerThanPattern(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poiboiboiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboi";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(3,location);
    }
    //32>Text>Pattern
    @Test
    public void testBNDMWildcardsCI32LargerThanTextLargerThanPattern(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poipoiboiboiboiboi";
  		String pattern = "boiboi";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(6,location);
    }
    
    
    //The following 3 test cases are based on the inputs to BNDMWildcardsCI
    //implementation of StringSearch.searchString that contains
    //this partition is about the location returned from the function searchBytes
    //there is 3 kinds of output -1, 0, >0
    
    //textstart and textend are both set to default value(start=0, end=text.length())
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCILocationIsNegativeOneInBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
    	byte[] text = new byte[]{1,2,3,4,5};
    	byte[] pattern = new byte[]{2,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 0 means can find pattern in text and the found position is at the beginning
    @Test
    public void testBNDMWildcardsCILocationIsZeroInBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
    	byte[] text = new byte[]{1,2,3,4,5};
    	byte[] pattern = new byte[]{1,2,3};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(0, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 6 means can find pattern in text and the found position is 6 in text
    @Test
    public void testBNDMWildcardsCILocationIsPositiveInBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
    	byte[] text = new byte[]{1,2,3,4,5};
    	byte[] pattern = new byte[]{3,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(2, location);
    }
    
    //The following 3 test cases are based on the inputs to BNDMWildcardsCI
    //implementation of StringSearch.searchBytes that contains
    //this partition is about the relation of the values of TextStart and TextEnd and pattern
    //there are 7 blocks of inputs
    //(TextStart<TextEnd)&(pattern is totally in this part), the final output should be the right position
    //(TextStart<TextEnd)&(pattern has some part included in this part), the final output should be -1
    //(TextStart<TextEnd)&(pattern has no part included in this part), the final output should be -1
    //(TextStart==TextEnd), whatever the pattern is, the final output should be the -1
    // TextStart>TextEnd, whatever the pattern is, it will always not in this part and return should be -1
    
   
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is 2 means can find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternIsTotallyInThisPartInBytes(){
    	    StringSearch ss = new BNDMWildcardsCI();
       		String text = "poipoiboi";
       		String pattern = "ip";
       		int textstart = 2;
       		int textend = 7;
       		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
       		assertEquals(2, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is partially in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternHasSomePartsIncludedInThisPartInBytes(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboi";
   		String pattern = "poib";
   		int textstart = 7;
   		int textend = 7;
   		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern has no part in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternHasNoPartIncludedInThisPartInBytes(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboi";
   		String pattern = "boi";
   		int textstart = 2;
   		int textend = 5;
   		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is -1 means can find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartEqualsTextEndAndPatternIsTotallyInThisPartInBytes(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboipoipoiboi";
   		String pattern = "poipoiboi";
   		int textstart = 0;
   		int textend = 0;
   		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
   		System.out.println(location);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart larger than TextEnd
    //the location returned is -1 means can find pattern in text
    @Test
   	public void testBNDMWildcardsCITextStartLargerThanTextEndInBytes(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "poipoiboi";
  		String pattern = "boi";
  		int textstart = 5;
  		int textend = 4;
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
  		
  }
  //the following test cases are based on the functionality aspect of the length
    //of text and pattern based on the inputs to BNDMWildcardsCI implementation of StringSearch.searchString
	//this partition is about the relation among the length of Text and pattern and 32 bytes
    
    //blocks are like:
    //Text<Pattern<32
    //Text<32<Pattern
    //32<Text<Pattern
    //Text=Pattern<32
    //Text=Pattern=32
    //Text=Pattern>32
    //Text>Pattern>32
    //Text>32>Pattern
    //32>Text>Pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart, TextEnd reasonable input
    //the location returned is -1 means can find pattern in text
    
    
    //Text<Pattern<32
    @Test
   	public void testBNDMWildcardsCITextSmallerThanPatternSmallerThan32InBytes(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "boi";
  		String pattern = "poipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
  		}
    //Text<32<Pattern
    @Test
   	public void testBNDMWildcardsCITextSmallerThan32SmallerThanPatternInBytes(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "boi";
  		String pattern = "poipoiboipoipoiboipoipoiboipoipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
  		}
    
    //32<Text<Pattern
    @Test
    public void testBNDMWildcardsCI32SmallerThanTextSmallerThanPatternInBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "poipoiboipoipoiboipoipoiboipoipoiboiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
  		
    }
    
    //Text=Pattern<32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternSmallerThan32InBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboi";
  		String pattern = "poipoiboipoi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
    }
    
    //Text=Pattern=32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternEqualsTo32InBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboibo";
  		String pattern = "poipoiboipoipoipoiboipoipoipoibo";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
    }
    
    //Text=Pattern>32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternLargerThan32InBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboiboiboiboiboiboiboipppppp";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(0,location);
    }
    
    //Text>Pattern>32
    //when pattern's length > 32, the processChars() will only deal with the first 32 bytes
    //in this string, so in this case, the output actually not the one it should be
    @Test
    public void testBNDMWildcardsCITextLargerThanPatternLargerThan32InBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poiboiboiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboiboiboiboiboiboiboipppppp";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(3,location);
    }
    
    //Text>32>Pattern
    @Test
    public void testBNDMWildcardsCITextLargerThan32LargerThanPatternInBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poiboiboiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(3,location);
    }
    //32>Text>Pattern
    @Test
    public void testBNDMWildcardsCI32LargerThanTextLargerThanPatternInBytes(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poipoiboiboiboiboi";
  		String pattern = "boiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(6,location);
    }
    
    
    
    
  //The following 3 test cases are based on the inputs to BNDMWildcardsCI
    //implementation of StringSearch.searchString that contains
    //this partition is about the location returned from the function searchBytes
    //there is 3 kinds of output -1, 0, >0
    
    //textstart and textend are both set to default value(start=0, end=text.length())
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCILocationIsNegativeOneInChars(){
    	StringSearch ss = new BNDMWildcardsCI();
    	char[] text = new char[]{1,2,3,4,5};
    	char[] pattern = new char[]{2,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processChars(pattern);
    	int location = ss.searchChars(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 0 means can find pattern in text and the found position is at the beginning
    @Test
    public void testBNDMWildcardsCILocationIsZeroInChars(){
    	StringSearch ss = new BNDMWildcardsCI();
    	char[] text = new char[]{1,2,3,4,5};
    	char[] pattern = new char[]{1,2,3};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processChars(pattern);
    	int location = ss.searchChars(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(0, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 6 means can find pattern in text and the found position is 6 in text
    @Test
    public void testBNDMWildcardsCILocationIsPositiveInChars(){
    	StringSearch ss = new BNDMWildcardsCI();
    	char[] text = new char[]{1,2,3,4,5};
    	char[] pattern = new char[]{3,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processChars(pattern);
    	int location = ss.searchChars(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(2, location);
    }
    
    //The following 3 test cases are based on the inputs to BNDMWildcardsCI
    //implementation of StringSearch.searchBytes that contains
    //this partition is about the relation of the values of TextStart and TextEnd and pattern
    //there are 7 blocks of inputs
    //(TextStart<TextEnd)&(pattern is totally in this part), the final output should be the right position
    //(TextStart<TextEnd)&(pattern has some part included in this part), the final output should be -1
    //(TextStart<TextEnd)&(pattern has no part included in this part), the final output should be -1
    //(TextStart==TextEnd), whatever the pattern is, the final output should be the -1
    // TextStart>TextEnd, whatever the pattern is, it will always not in this part and return should be -1
    
   
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is 2 means can find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternIsTotallyInThisPartInChars(){
    	    StringSearch ss = new BNDMWildcardsCI();
       		String text = "poipoiboi";
       		String pattern = "ip";
       		int textstart = 2;
       		int textend = 7;
       		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
       		assertEquals(2, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is partially in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternHasSomePartsIncludedInThisPartInChars(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboi";
   		String pattern = "poib";
   		int textstart = 7;
   		int textend = 7;
   		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern has no part in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartLessThanTextEndAndPatternHasNoPartIncludedInThisPartInChars(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboi";
   		String pattern = "boi";
   		int textstart = 2;
   		int textend = 5;
   		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is -1 means can find pattern in text
    @Test
    public void testBNDMWildcardsCITextStartEqualsTextEndAndPatternIsTotallyInThisPartInChars(){
	    StringSearch ss = new BNDMWildcardsCI();
   		String text = "poipoiboipoipoiboi";
   		String pattern = "poipoiboi";
   		int textstart = 0;
   		int textend = 0;
   		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
   		System.out.println(location);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart larger than TextEnd
    //the location returned is -1 means can find pattern in text
    @Test
   	public void testBNDMWildcardsCITextStartLargerThanTextEndInChars(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "poipoiboi";
  		String pattern = "boi";
  		int textstart = 5;
  		int textend = 4;
  		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
  		assertEquals(-1,location);
  		
  }
  //the following test cases are based on the functionality aspect of the length
    //of text and pattern based on the inputs to BNDMWildcardsCI implementation of StringSearch.searchString
	//this partition is about the relation among the length of Text and pattern and 32 bytes
    
    //blocks are like:
    //Text<Pattern<32
    //Text<32<Pattern
    //32<Text<Pattern
    //Text=Pattern<32
    //Text=Pattern=32
    //Text=Pattern>32
    //Text>Pattern>32
    //Text>32>Pattern
    //32>Text>Pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart, TextEnd reasonable input
    //the location returned is -1 means can find pattern in text
    
    
    //Text<Pattern<32
    @Test
   	public void testBNDMWildcardsCITextSmallerThanPatternSmallerThan32InChar(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "boi";
  		String pattern = "poipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
  		assertEquals(-1,location);
  		}
    //Text<32<Pattern
    @Test
   	public void testBNDMWildcardsCITextSmallerThan32SmallerThanPatternInChar(){
	    StringSearch ss = new BNDMWildcardsCI();
  		String text = "boi";
  		String pattern = "poipoiboipoipoiboipoipoiboipoipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
  		}
    
    //32<Text<Pattern
    @Test
    public void testBNDMWildcardsCI32SmallerThanTextSmallerThanPatternInChar(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "poipoiboipoipoiboipoipoiboipoipoiboiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
  		
    }
    
    //Text=Pattern<32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternSmallerThan32InChar(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboi";
  		String pattern = "poipoiboipoi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
    }
    
    //Text=Pattern=32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternEqualsTo32InChar(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboibo";
  		String pattern = "poipoiboipoipoipoiboipoipoipoibo";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
    }
    
    //Text=Pattern>32
    @Test
    public void testBNDMWildcardsCITextEqualsToPatternLargerThan32InChar(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "boiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboiboiboiboiboiboiboipppppp";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(0,location);
    }
    
    //Text>Pattern>32
    //when pattern's length > 32, the processChars() will only deal with the first 32 bytes
    //in this string, so in this case, the output actually not the one it should be
    @Test
    public void testBNDMWildcardsCITextLargerThanPatternLargerThan32InChar(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poiboiboiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboiboiboiboiboiboiboipppppp";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(3,location);
    }
    
    //Text>32>Pattern
    @Test
    public void testBNDMWildcardsCITextLargerThan32LargerThanPatternInChar(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poiboiboiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(3,location);
    }
    //32>Text>Pattern
    @Test
    public void testBNDMWildcardsCI32LargerThanTextLargerThanPatternInChar(){
    	StringSearch ss = new BNDMWildcardsCI();
  		String text = "poipoiboiboiboiboi";
  		String pattern = "boiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(6,location);
    }
	// Technique used: input space partitioning
	//
	// The following three test cases represents the block of inputs 
	// to the BNDMWildcardsCI implementation of StringSearch.searchString
	// that each contains:
	//
	// a non-null pattern, and a non-null text, and
	// the pattern does not occur in the text or
	// the pattern occurs once in the text or
	// the pattern occurs more than once in the text
    @Test
    public void testBNDMWildcardsCIStringZeroDuplication(){
    	StringSearch ss = new BNDMWildcardsCI();
    	String text = "duplicate";
    	String pattern = "no";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBNDMWildcardsCIStringOneDuplication(){
    	StringSearch ss = new BNDMWildcardsCI();
    	String text = "once a while";
    	String pattern = "once";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testBNDMWildcardsCIStringMoreDuplications(){
    	StringSearch ss = new BNDMWildcardsCI();
    	String text = "two for two";
    	String pattern = "two";
    	Object processed = ss.processString(pattern);
    	int locationA = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	int locationB = ss.searchString(text, locationA, text.length() - 1, pattern, processed);
    	assertEquals(pattern.charAt(locationA), pattern.charAt(locationB));
    }
    
    // Technique used: input space partitioning
	//
	// The following two test cases represents the block of inputs 
    // to the BNDMWildcardsCI implementation of StringSearch.searchString
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern is not found in the text or
 	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testBNDMWildcardsCIStringNoVerification(){
    	StringSearch ss = new BNDMWildcardsCI();
    	String text = "duplicate";
    	String pattern = "no";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBNDMWildcardsCIStringVerification(){
    	StringSearch ss = new BNDMWildcardsCI();
    	String text = "once a while";
    	String pattern = "once";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(text.charAt(location), pattern.charAt(0));
    }
    
	// Technique used: input space partitioning
	//
	// The following three test cases represents the block of inputs 
	// to the BNDMWildcardsCI implementation of StringSearch.searchChars
	// that each contains:
	//
	// a non-null pattern, and a non-null text, and
	// the pattern does not occur in the text or
	// the pattern occurs once in the text or
	// the pattern occurs more than once in the text
    @Test
    public void testBNDMWildcardsCICharsZeroDuplication(){
    	StringSearch ss = new BNDMWildcardsCI();
    	char[] text = {'d','u','p','l','i','c','a','t','e'};
    	char[] pattern = {'n','o'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBNDMWildcardsCICharsOneDuplication(){
    	StringSearch ss = new BNDMWildcardsCI();
    	char[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	char[] pattern = {'o','n','c','e'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testBNDMWildcardsCICharsMoreDuplications(){
    	StringSearch ss = new BNDMWildcardsCI();
    	char[] text = {'t','w','o',' ','f','o','r',' ','t','w','o'};
    	char[] pattern = {'t','w','o'};
    	Object processed = ss.processChars(pattern);
    	int locationA = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	int locationB = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[locationA], text[locationB]);
    }
    
    // Technique used: input space partitioning
	//
	// The following two test cases represents the block of inputs 
    // to the BNDMWildcardsCI implementation of StringSearch.searchChars
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern is not found in the text or
 	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testBNDMWildcardsCICharsNoVerification(){
    	StringSearch ss = new BNDMWildcardsCI();
    	char[] text = {'d','u','p','l','i','c','a','t','e'};
    	char[] pattern = {'n','o'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBNDMWildcardsCICharsVerification(){
    	StringSearch ss = new BNDMWildcardsCI();
    	char[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	char[] pattern = {'o','n','c','e'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[location], pattern[0]);
    }
    
    // Technique used: input space partitioning
 	//
 	// The following three test cases represents the block of inputs 
 	// to the BNDMWildcardsCI implementation of StringSearch.searchBytes
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern does not occur in the text or
 	// the pattern occurs once in the text or
 	// the pattern occurs more than once in the text
    @Test
    public void testBNDMWildcardsCIBytesZeroDuplication(){
    	StringSearch ss = new BNDMWildcardsCI();
    	byte[] text = {'d','u','p','l','i','c','a','t','e'};
    	byte[] pattern = {'n','o'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBNDMWildcardsCIBytesOneDuplication(){
    	StringSearch ss = new BNDMWildcardsCI();
    	byte[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	byte[] pattern = {'o','n','c','e'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testBNDMWildcardsCIBytesMoreDuplications(){
    	StringSearch ss = new BNDMWildcardsCI();
    	byte[] text = {'t','w','o',' ','f','o','r',' ','t','w','o'};
    	byte[] pattern = {'t','w','o'};
    	Object processed = ss.processBytes(pattern);
    	int locationA = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	int locationB = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[locationA], text[locationB]);
    }
    
    // Technique used: input space partitioning
  	//
  	// The following two test cases represents the block of inputs 
    // to the BNDMWildcardsCI implementation of StringSearch.searchBytes
   	// that each contains:
   	//
   	// a non-null pattern, and a non-null text, and
   	// the pattern is not found in the text or
   	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testBNDMWildcardsCIBytesNoVerification(){
    	StringSearch ss = new BNDMWildcardsCI();
    	byte[] text = {'d','u','p','l','i','c','a','t','e'};
    	byte[] pattern = {'n','o'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBNDMWildcardsCIBytesVerification(){
    	StringSearch ss = new BNDMWildcardsCI();
    	byte[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	byte[] pattern = {'o','n','c','e'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[location], pattern[0]);
    }
}

