package com.eaio.stringsearch.inputSpaceTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.eaio.stringsearch.BoyerMooreHorspool;
import com.eaio.stringsearch.MismatchSearch;
import com.eaio.stringsearch.ShiftOrMismatches;
import com.eaio.stringsearch.StringSearch;

public class InterfBasedTestForMS {
	private static int NOT_FOUND = -1;
	private static int NOT_MATCHED = 0;
	private static int FULL_MATCHED = 2;
	private MismatchSearch ssMS;
	private int[] locs = {Integer.MIN_VALUE, Integer.MIN_VALUE};
	
	@Before
	public void init() {
		ssMS = new ShiftOrMismatches();
		locs[0] = Integer.MIN_VALUE;
		locs[1] = Integer.MIN_VALUE;
	}
	
	/* This test suite is used to perform interface-based input space test.
	 * To ignore those wrapper function, this test pick the rawest three:
	 * 		int searchBytes(byte[] text, int textStart, int textEnd,
     *                              byte[] pattern, Object processed, int k);
     *      int searchChars(char[] text, int textStart, int textEnd,
     *                              char[] pattern, Object processed, int k);
	 * 		int searchString(String text, int textStart, int textEnd,
     *                            String pattern, Object processed, int k);
     *                            
     * There are five similar parameters for each function.
     * 		For "text":
     * 			Block1 (IsNull) : Null
     * 			Block2 (IsEmpty): Not null, but empty.
     * 			Block3 (HasCtnt): Neither null nor empty.
     * 		For "testStart":
     * 			Block1 (LTZero)	: (-infinity, 0) 
     * 			Block2 (IsZero)	: [0, 0]
     * 			Block3 (GTZero) : (0, +infinity)
     * 		For "textEnd":
     * 			Block1 (LTZero)	: (-infinity, 0) 
     * 			Block2 (IsZero)	: [0, 0]
     * 			Block3 (GTZero) : (0, +infinity)
     * 		For "pattern":
     * 			Block1 (IsNull) : Null
     * 			Block2 (IsEmpty): Not null, but empty.
     * 			Block3 (HasCtnt): Neither null nor empty
     * 		For "processed":
     * 			Block1 (IsNull) : Null
     * 			Block2 (NtNull) : Not Null
     * 		For "k":
     * 			Block1 (LTZero) : k < 0
     * 			Block2 (IsZero) : k = 0
     * 			Block3 (GTZero) : k > 0
     * 
     * 		For all combination situation, there should be 546 test cases.
     * 		But those hundreds of test cases may have redundancies.
     * 		Thus, we would like to use "Base choice method" to cover the most
     * 		common situation with fewer cases.
     * 
     *  	The base choice is: {B3, B3, B3, B3, B2, B3} 
     *  	(This combination is considered as the most frequent situation)
     *  
     *  	Then, the others are:
     *  	C01: {B1, B3, B3, B3, B2, B3} C02: {B2, B3, B3, B3, B2, B3} 
     *  	C03: {B3, B1, B3, B3, B2, B3} C04: {B3, B2, B3, B3, B2, B3} 
     *  	C05: {B3, B3, B1, B3, B2, B3} C06: {B3, B3, B2, B3, B2, B3} 
     *  	C07: {B3, B3, B3, B1, B2, B3} C08: {B3, B3, B3, B2, B2, B3} 
     *  	C09: {B3, B3, B3, B3, B1, B3}
     *  	C10: {B3, B3, B3, B3, B2, B1} C11: {B3, B3, B3, B3, B2, B2}  
     *  
     *  	*Notion: 
     *  		All test cases take the instruction of function 
     *  		as the requirement and are designed according to it.
	 */

	/* For txt and ptn are the type of String  */
    //C00: {B3, B3, B3, B3, B1, B3}
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

    //C01: {B1, B3, B3, B3, B1, B3}
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtIsNull_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = null;
		int tS = 2;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
	}
	
	//C02: {B2, B3, B3, B3, B1, B3} TODO: Failure: ArrayIndexOutOfBoundsException on "searchString"
	@Ignore
	public void ist_ssMS_txtIsEmpty_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "";
		int tS = 2;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
    //C03: {B3, B1, B3, B3, B1, B3} TODO: Failure: ArrayIndexOutOfBoundsException on "searchString"
	@Ignore
	public void ist_ssMS_txtHasCtnt_tSLTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = -1;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);
		
		assertTrue(tS < 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}
	
	//C04: {B3, B2, B3, B3, B1, B3} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSIsZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);
		
		assertTrue(tS == 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}
	
    //C05: {B3, B3, B1, B3, B1, B3} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tELTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = -1;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);
		
		assertTrue(tS > 0);	
		assertTrue(tE < 0);	 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
	//C06: {B3, B3, B2, B3, B1, B3}
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEIsZero_ptnHasCtnt_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 0;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);
		
		assertTrue(tS > 0);
		assertTrue(tE == 0);	 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
    //C07: {B3, B3, B3, B1, B1, B3} 
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnIsNull_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = null;
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
	}
	
	//C08: {B3, B3, B3, B2, B1, B3} TODO: Here is a failure: Cannot covert empty string.
	@Ignore
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnIsEmpty_prcdNtNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
    //C09: {B3, B3, B3, B3, B2, B3}
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdIsNull_kGTZero_ForStr(){
		int k = 2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = null;

		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchString(txt, tS, tE, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
	} 
	
	//C10: {B3, B3, B3, B3, B2, B1} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kLTZero_ForStr(){
		int k = -2;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
	//C11: {B3, B3, B3, B3, B2, B2} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kIsZero_ForStr(){
		int k = 0;
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "Suin";
		Object pObj = ssMS.processChars(ptn.toCharArray(), k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchString(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
	/*
	 *
	 */

	/* For txt and ptn are the type of byte[]  */
    //C00: {B3, B3, B3, B3, B1, B3}
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

    //C01: {B1, B3, B3, B3, B1, B3}
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtIsNull_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = null;
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);	
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
	}
	
	//C02: {B2, B3, B3, B3, B1, B3} TODO: Failure: ArrayIndexOutOfBoundsException on "searchBytes"
	@Ignore
	public void ist_ssMS_txtIsEmpty_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);	
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
    //C03: {B3, B1, B3, B3, B1, B3} TODO: Failure: ArrayIndexOutOfBoundsException on "searchBytes"
	@Ignore
	public void ist_ssMS_txtHasCtnt_tSLTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = -1;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		
		assertTrue(tS < 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}
	
	//C04: {B3, B2, B3, B3, B1, B3} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSIsZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		
		assertTrue(tS == 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}
		
    //C05: {B3, B3, B1, B3, B1, B3} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tELTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = -1;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE < 0);
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
	//C06: {B3, B3, B2, B3, B1, B3}
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEIsZero_ptnHasCtnt_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 0;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE == 0);
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
		
    //C07: {B3, B3, B3, B1, B1, B3} 
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnIsNull_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = null;
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
	}
	
	//C08 {B3, B3, B3, B2, B1, B3} TODO: Here is a failure: Cannot covert empty string.
	@Ignore
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnIsEmpty_prcdNtNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
		
    //C09: {B3, B3, B3, B3, B2, B3}
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdIsNull_kGTZero_ForBytes(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = null;

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		locs = ssMS.searchBytes(txt, tS, tE, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
	} 

	//C10: {B3, B3, B3, B3, B2, B1} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kLTZero_ForBytes(){
		int k = -2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
	//C11: {B3, B3, B3, B3, B2, B2}  
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kIsZero_ForBytes(){
		int k = 0;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssMS.processBytes(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchBytes(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
	/*
	 *
	 */

	/* For txt and ptn are the type of char[]  */
    //C00: {B3, B3, B3, B3, B1, B3}
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}

    //C01: {B1, B3, B3, B3, B1, B3}
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtIsNull_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = null;
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);	
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
	}
	
	//C02: {B2, B3, B3, B3, B1, B3} TODO: Failure: ArrayIndexOutOfBoundsException on "searchChars"
	@Ignore
	public void ist_ssMS_txtIsEmpty_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = "";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);	
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
    //C03: {B3, B1, B3, B3, B1, B3} TODO: Failure: ArrayIndexOutOfBoundsException on "searchChars"
	@Ignore
	public void ist_ssMS_txtHasCtnt_tSLTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = -1;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		
		assertTrue(tS < 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}
	
	//C04: {B3, B2, B3, B3, B1, B3} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSIsZero_tEGTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		
		assertTrue(tS == 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
	}
		
    //C05: {B3, B3, B1, B3, B1, B3} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tELTZero_ptnHasCtnt_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = -1;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE < 0);
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
	//C06: {B3, B3, B2, B3, B1, B3}
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEIsZero_ptnHasCtnt_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 0;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE == 0);
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
		
    //C07: {B3, B3, B3, B1, B1, B3} 
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnIsNull_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = null;
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);

		assertTrue(tS > 0);
		assertTrue(tE > 0); 
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
	}
	
	//C08: {B3, B3, B3, B2, B1, B3} TODO: Here is a failure: Cannot covert empty string.
	@Ignore
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnIsEmpty_prcdNtNull_kGTZero_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
    //C09: {B3, B3, B3, B3, B2, B3}
	@Test(expected = NullPointerException.class)
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdIsNull_kGTZero_ForChars(){
		int k = 2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = null;

		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchChars(txt, tS, tE, ptn, k);
		assertTrue(locs[0] == 13);
		assertTrue(locs[1] == FULL_MATCHED);
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
	} 

	//C10: {B3, B3, B3, B3, B2, B1} 
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kLTZero_ForChars(){
		int k = -2;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
	//C11: {B3, B3, B3, B3, B2, B2}  
	@Test
	public void ist_ssMS_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_kIsZero_ForChars(){
		int k = 0;
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "Suin";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssMS.processChars(ptn, k);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		locs = ssMS.searchChars(txt, tS, tE, ptn, pObj, k);
		assertTrue(locs[0] == NOT_FOUND);
		assertTrue(locs[1] == NOT_MATCHED);
	}
	
}