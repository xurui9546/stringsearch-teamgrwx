package com.eaio.stringsearch.inputSpaceTest;
import static org.junit.Assert.*;

import org.junit.Test;

import com.eaio.stringsearch.BoyerMooreHorspool;
import com.eaio.stringsearch.StringSearch;
public class BoyerMooreHorspoolTest {

  	@Test
  	public void hasNoIntersectionForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text and pattern have no intersection.
  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwertyuio";
  		String pattern = "qoiue";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void hasIntersectionForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text and pattern have intersection but not equal
  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "ertyui";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void textEqualToPatternForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text and pattern are equals
  		
  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "qwerty";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(0, location);
  	}
  	
  	@Test
  	public void textContainsPatternForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text contains pattern
  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "rty";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(3, location);
  	}
  	
  	@Test
  	public void patternContainsTestForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - pattern contains text
  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "rty";
  		String pattern = "qwerty";
  		
  		int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
    //text and pattern are all lower case
  	//this case is same with above test cases
  	
  	@Test
  	public void testLowerTextAndLowerAndUpperPatternForString() {
  		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text is lower case and pattern contains upper case and lower case chars  		
  	  	StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "qwerty";
  	  	String pattern = "rTy";
  	  
  	  	int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  	  	assertEquals(-1, location);
  	}
  	@Test
  	public void testLowerAndUpperTextAndLowerPatternForString() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text contains upper case and lower case chars and pattern is lower case
  	  	StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "qwerTy";
  	  	String pattern = "rty";
  	  
  	  	int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  	  	assertEquals(-1, location);
  	}
  	
  	@Test
  	public void testUpperTextAndUpperPatternForString() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a non-null pattern, a non-null string, two integers and an object
  		// - text and pattern are all upper case
  		StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "QWERTY";
  	  	String pattern = "RTY";
  	  
  	  	int location = ss.searchString(str,0,str.length(),pattern,ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}

  	@Test
  	public void hasNoIntersectionForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text and pattern have no intersection

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwertyuio";
  		String pattern = "qoiue";
  		
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void hasIntersectionForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text and pattern have intersection
  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "ertyui";
  		
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void textEqualToPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text and pattern are equal

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "qwerty";
  		
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(0, location);
  	}
  	
  	@Test
  	public void textContainsPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text contains pattern

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "rty";
  		
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(3, location);
  	}
  	
  	@Test
  	public void patternContainsTestForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - pattern contains text

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "rty";
  		String pattern = "qwerty";
  		int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  		assertEquals(-1, location);
  	}
    //text and pattern are all lower case
  	//this case is same with above test cases
  	
  	@Test
  	public void testLowerTextAndLowerAndUpperPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text is lower case and pattern contains upper case and lower case chars

  	  	StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "qwerty";
  	  	String pattern = "rTy";
  	  
  	  int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(-1, location);
  	}

  	@Test
  	public void testLowerAndUpperTextAndLowerPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text contains upper case and lower case chars and pattern is lower case

  	  	StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "qwerTy";
  	  	String pattern = "rty";
  	  
  	  int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(-1, location);
  	}
  	
  	@Test
  	public void testUpperTextAndUpperPatternForBytes() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a bytes array pattern, a bytes array text, two integers and an object
  		// - text and pattern are all upper case

  		StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "QWERTY";
  	  	String pattern = "RTY";
  	  
  	  int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(3, location);
  	}
  	
  	@Test
  	public void hasNoIntersectionForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text and pattern have no intersection

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwertyuio";
  		String pattern = "qoiue";
  		
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void hasIntersectionForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text and pattern have intersection

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "ertyui";
  		
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
  	
  	@Test
  	public void textEqualToPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text and pattern are equal

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "qwerty";
  		
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(0, location);
  	}
  	
  	@Test
  	public void textContainsPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text contains pattern

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "qwerty";
  		String pattern = "rty";
  		
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(3, location);
  	}
  	
  	@Test
  	public void patternContainsTestForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - pattern contains text

  		StringSearch ss = new BoyerMooreHorspool();
  		String str = "rty";
  		String pattern = "qwerty";
  		int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  		assertEquals(-1, location);
  	}
    //text and pattern are all lower case
  	//this case is same with above test cases
  	
  	@Test
  	public void testLowerTextAndLowerAndUpperPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text is lower case and pattern contains upper case and lower case chars

  	  	StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "qwerty";
  	  	String pattern = "rTy";
  	  
  	    int location = ss.searchBytes(str.getBytes(),0,str.length(), pattern.getBytes(),ss.processBytes(pattern.getBytes()));
  	  	assertEquals(-1, location);
  	}
  	@Test
  	public void testLowerAndUpperTextAndLowerPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text contains upper case and lower case chars and pattern is lower case

  	  	StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "qwerTy";
  	  	String pattern = "rty";
  	  
  	    int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  	  	assertEquals(-1, location);
  	}
  	
  	@Test
  	public void testUpperTextAndUpperPatternForChars() {
 		// Technique used: input space partitioning
  		//
  		// This test case represents the block of inputs to the
  		// BoyerMooreHorspool implementation of StringSearch.searchString
  		// that contain:
  		//
  		// - a char array pattern, a char array text, two integers and an object
  		// - text and pattern are all upper case

  		StringSearch ss = new BoyerMooreHorspool();
  	  	String str = "QWERTY";
  	  	String pattern = "RTY";
  	  
  	    int location = ss.searchChars(str.toCharArray(),0,str.length(), pattern.toCharArray(),ss.processChars(pattern.toCharArray()));
  	  	assertEquals(3, location);
  	}
	//The following 3 test cases are based on the inputs to BoyerMooreHorspool
    //implementation of StringSearch.searchString that contains
    
    //-a non null pattern
    //-a non null text
    //the location returned is -1 means cannot find pattern in text
	//The following 3 test cases are based on the functionality aspect of the length
    //of text and pattern based on the inputs to BoyerMooreHorspool implementation of StringSearch.searchString
	//this partition is about the relation between the length of text and pattern
	//there are 3 kinds of blocks: the length of text is longer, their length are the same, the length of pattern is longer
    
	//textstart and textend are both set to default value(start=0, end=text.length())
    //the pre-processed object is corresponding to the input of pattern
	
	//-a non null pattern
    //-a non null text
	//the length of text is longer than that of pattern
    //the location returned is 11 means can find pattern in text
	@Test
    public void testBoyerMooreHorspoolLengthOfTextLongerThanPattern(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "we are the best";
    	String pattern = "best";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(11, location);
    	}
	
	//-a non null pattern
    //-a non null text
	//the length of text equals to that of pattern
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolLengthOfTextequalsPattern(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "nice";
    	String pattern = "ecin";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
	//the length of text is shorter than that of pattern
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolLengthOfTextShorterThanPattern(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "best";
    	String pattern = "we are the best";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(-1, location);
    }
    
    
    
    //The following 3 test cases are based on the inputs to BoyerMooreHorspool
    //implementation of StringSearch.searchString that contains
    //this partition is about the location returned from the function searchString
    //there is 3 kinds of output -1, 0, >0
    
    //textstart and textend are both set to default value(start=0, end=text.length())
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolLocationIsNegativeOne(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "fault";
    	String pattern = "right";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 0 means can find pattern in text and the found position is at the beginning
    @Test
    public void testBoyerMooreHorspoolLocationIsZero(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "right tiger";
    	String pattern = "right";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(0, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 6 means can find pattern in text and the found position is 6 in text
    @Test
    public void testBoyerMooreHorspoolLocationIsPositive(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "right tiger";
    	String pattern = "tiger";
    	int textstart = 0;
    	int textend = text.length();
    	Object aa = ss.processChars(pattern.toCharArray());
    	int location = ss.searchString(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(6, location);
    }
	
  
	
    
    //The following 3 test cases are based on the inputs to BoyerMooreHorspool
    //implementation of StringSearch.searchString that contains
    //this partition is about the relation of the values of TextStart and TextEnd and pattern
    //there are 7 blocks of inputs
    //(TextStart<TextEnd)&(pattern is totally in this part), the final output should be the right position
    //(TextStart<TextEnd)&(pattern has some part included in this part), the final output should be -1
    //(TextStart<TextEnd)&(pattern has no part included in this part), the final output should be -1
    //(TextStart==TextEnd), whatever the pattern is, the final output should be the -1
    // TextStart>TextEnd, whatever the pattern is, it will always not in this part and return should be -1
    
   
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is 2 means can find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternIsTotallyInThisPart(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboi";
   		String pattern = "ip";
   		int textstart = 2;
   		int textend = 7;
   		Object aa = ss.processChars(pattern.toCharArray());
   		int location = ss.searchString(text, textstart, textend, pattern, aa);
   		assertEquals(2, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is partially in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternHasSomePartsIncludedInThisPart(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboi";
   		String pattern = "poib";
   		int textstart = 7;
   		int textend = 7;
   		Object aa = ss.processChars(pattern.toCharArray());
   		int location = ss.searchString(text, textstart, textend, pattern, aa);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern has no part in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternHasNoPartIncludedInThisPart(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboi";
   		String pattern = "boi";
   		int textstart = 2;
   		int textend = 5;
   		Object aa = ss.processChars(pattern.toCharArray());
   		int location = ss.searchString(text, textstart, textend, pattern, aa);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is -1 means can find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartEqualsTextEnd(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboipoipoiboi";
   		String pattern = "poipoiboi";
   		int textstart = 0;
   		int textend = 0;
   		Object aa = ss.processChars(pattern.toCharArray());
   		int location = ss.searchString(text, textstart, textend, pattern, aa);
   		System.out.println(location);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart larger than TextEnd
    //the location returned is -1 means can find pattern in text
    @Test
   	public void testBoyerMooreHorspoolTextStartLargerThanTextEnd(){
	    StringSearch ss = new BoyerMooreHorspool();
  		String text = "poipoiboi";
  		String pattern = "boi";
  		int textstart = 5;
  		int textend = 4;
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
  		
  }
    //the following test cases are based on the functionality aspect of the length
    //of text and pattern based on the inputs to BoyerMooreHorspool implementation of StringSearch.searchString
	//this partition is about the relation among the length of Text and pattern
    
    //blocks are like:
    //Text<Pattern
    //Text=Pattern
    //Text>Pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart, TextEnd reasonable input
    //the location returned is -1 means can find pattern in text
    
    
    //Text<Pattern
    @Test
   	public void testBoyerMooreHorspoolTextSmallerThanPatternSmallerThan32(){
	    StringSearch ss = new BoyerMooreHorspool();
  		String text = "boi";
  		String pattern = "poipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
  		}
    
    
    
    //Text=Pattern
    @Test
    public void testBoyerMooreHorspoolTextEqualsToPatternEqualsTo32(){
    	StringSearch ss = new BoyerMooreHorspool();
  		String text = "boiboiboiboiboiboiboiboiboiboibo";
  		String pattern = "poipoiboipoipoipoiboipoipoipoibo";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(-1,location);
    }
    
   
    //Text>Pattern
    @Test
    public void testBoyerMooreHorspool32LargerThanTextLargerThanPattern(){
    	StringSearch ss = new BoyerMooreHorspool();
  		String text = "poipoiboiboiboiboi";
  		String pattern = "boiboi";
  		int textstart = 0;
  		int textend = text.length();
  		Object aa = ss.processChars(pattern.toCharArray());
  		int location = ss.searchString(text, textstart, textend, pattern, aa);
  		assertEquals(6,location);
    }
    
    
    //The following 3 test cases are based on the inputs to BoyerMooreHorspool
    //implementation of StringSearch.searchString that contains
    //this partition is about the location returned from the function searchBytes
    //there is 3 kinds of output -1, 0, >0
    
    //textstart and textend are both set to default value(start=0, end=text.length())
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolLocationIsNegativeOneInBytes(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = new byte[]{1,2,3,4,5};
    	byte[] pattern = new byte[]{2,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 0 means can find pattern in text and the found position is at the beginning
    @Test
    public void testBoyerMooreHorspoolLocationIsZeroInBytes(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = new byte[]{1,2,3,4,5};
    	byte[] pattern = new byte[]{1,2,3};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(0, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 6 means can find pattern in text and the found position is 6 in text
    @Test
    public void testBoyerMooreHorspoolLocationIsPositiveInBytes(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = new byte[]{1,2,3,4,5};
    	byte[] pattern = new byte[]{3,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(2, location);
    }
    
    //The following 3 test cases are based on the inputs to BoyerMooreHorspool
    //implementation of StringSearch.searchBytes that contains
    //this partition is about the relation of the values of TextStart and TextEnd and pattern
    //there are 7 blocks of inputs
    //(TextStart<TextEnd)&(pattern is totally in this part), the final output should be the right position
    //(TextStart<TextEnd)&(pattern has some part included in this part), the final output should be -1
    //(TextStart<TextEnd)&(pattern has no part included in this part), the final output should be -1
    //(TextStart==TextEnd), whatever the pattern is, the final output should be the -1
    // TextStart>TextEnd, whatever the pattern is, it will always not in this part and return should be -1
    
   
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is 2 means can find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternIsTotallyInThisPartInBytes(){
    	    StringSearch ss = new BoyerMooreHorspool();
       		String text = "poipoiboi";
       		String pattern = "ip";
       		int textstart = 2;
       		int textend = 7;
       		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
       		assertEquals(2, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is partially in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternHasSomePartsIncludedInThisPartInBytes(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboi";
   		String pattern = "poib";
   		int textstart = 7;
   		int textend = 7;
   		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern has no part in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternHasNoPartIncludedInThisPartInBytes(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboi";
   		String pattern = "boi";
   		int textstart = 2;
   		int textend = 5;
   		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is -1 means can find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartEqualsTextEndAndPatternIsTotallyInThisPartInBytes(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboipoipoiboi";
   		String pattern = "poipoiboi";
   		int textstart = 0;
   		int textend = 0;
   		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
   		System.out.println(location);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart larger than TextEnd
    //the location returned is -1 means can find pattern in text
    @Test
   	public void testBoyerMooreHorspoolTextStartLargerThanTextEndInBytes(){
	    StringSearch ss = new BoyerMooreHorspool();
  		String text = "poipoiboi";
  		String pattern = "boi";
  		int textstart = 5;
  		int textend = 4;
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
  		
  }
  //the following test cases are based on the functionality aspect of the length
    //of text and pattern based on the inputs to BoyerMooreHorspool implementation of StringSearch.searchString
	//this partition is about the relation among the length of Text and pattern and 32 bytes
    
    //blocks are like:
    //Text<Pattern
    //Text=Pattern
    //Text>Pattern>
    
    //-a non null pattern
    //-a non null text
    //with TextStart, TextEnd reasonable input
    //the location returned is -1 means can find pattern in text
    
    
    //Text<Pattern
    @Test
   	public void testBoyerMooreHorspoolTextSmallerThanPatternSmallerThan32InBytes(){
	    StringSearch ss = new BoyerMooreHorspool();
  		String text = "boi";
  		String pattern = "poipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
  		}
    
    //Text=Pattern
    @Test
    public void testBoyerMooreHorspoolTextEqualsToPatternEqualsTo32InBytes(){
    	StringSearch ss = new BoyerMooreHorspool();
  		String text = "boiboiboiboiboiboiboiboiboiboibo";
  		String pattern = "poipoiboipoipoipoiboipoipoipoibo";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
    }
    
    
    //Text>Pattern
    @Test
    public void testBoyerMooreHorspoolTextLargerThan32LargerThanPatternInBytes(){
    	StringSearch ss = new BoyerMooreHorspool();
  		String text = "poiboiboiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(3,location);
    }
    
    
    
  //The following 3 test cases are based on the inputs to BoyerMooreHorspool
    //implementation of StringSearch.searchString that contains
    //this partition is about the location returned from the function searchBytes
    //there is 3 kinds of output -1, 0, >0
    
    //textstart and textend are both set to default value(start=0, end=text.length())
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolLocationIsNegativeOneInChars(){
    	StringSearch ss = new BoyerMooreHorspool();
    	char[] text = new char[]{1,2,3,4,5};
    	char[] pattern = new char[]{2,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processChars(pattern);
    	int location = ss.searchChars(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 0 means can find pattern in text and the found position is at the beginning
    @Test
    public void testBoyerMooreHorspoolLocationIsZeroInChars(){
    	StringSearch ss = new BoyerMooreHorspool();
    	char[] text = new char[]{1,2,3,4,5};
    	char[] pattern = new char[]{1,2,3};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processChars(pattern);
    	int location = ss.searchChars(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(0, location);
    }
    
    //-a non null pattern
    //-a non null text
    //the location returned is 6 means can find pattern in text and the found position is 6 in text
    @Test
    public void testBoyerMooreHorspoolLocationIsPositiveInChars(){
    	StringSearch ss = new BoyerMooreHorspool();
    	char[] text = new char[]{1,2,3,4,5};
    	char[] pattern = new char[]{3,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processChars(pattern);
    	int location = ss.searchChars(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(2, location);
    }
    
    //The following 3 test cases are based on the inputs to BoyerMooreHorspool
    //implementation of StringSearch.searchBytes that contains
    //this partition is about the relation of the values of TextStart and TextEnd and pattern
    //there are 7 blocks of inputs
    //(TextStart<TextEnd)&(pattern is totally in this part), the final output should be the right position
    //(TextStart<TextEnd)&(pattern has some part included in this part), the final output should be -1
    //(TextStart<TextEnd)&(pattern has no part included in this part), the final output should be -1
    //(TextStart==TextEnd), whatever the pattern is, the final output should be the -1
    // TextStart>TextEnd, whatever the pattern is, it will always not in this part and return should be -1
    
   
    //the pre-processed object is corresponding to the input of pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is 2 means can find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternIsTotallyInThisPartInChars(){
    	    StringSearch ss = new BoyerMooreHorspool();
       		String text = "poipoiboi";
       		String pattern = "ip";
       		int textstart = 2;
       		int textend = 7;
       		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
       		assertEquals(2, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is partially in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternHasSomePartsIncludedInThisPartInChars(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboi";
   		String pattern = "poib";
   		int textstart = 7;
   		int textend = 7;
   		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern has no part in this part
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartLessThanTextEndAndPatternHasNoPartIncludedInThisPartInChars(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboi";
   		String pattern = "boi";
   		int textstart = 2;
   		int textend = 5;
   		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart less than TextEnd and pattern is totally in this part
    //the location returned is -1 means can find pattern in text
    @Test
    public void testBoyerMooreHorspoolTextStartEqualsTextEndAndPatternIsTotallyInThisPartInChars(){
	    StringSearch ss = new BoyerMooreHorspool();
   		String text = "poipoiboipoipoiboi";
   		String pattern = "poipoiboi";
   		int textstart = 0;
   		int textend = 0;
   		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
   		System.out.println(location);
   		assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
    //with TextStart larger than TextEnd
    //the location returned is -1 means can find pattern in text
    @Test
   	public void testBoyerMooreHorspoolTextStartLargerThanTextEndInChars(){
	    StringSearch ss = new BoyerMooreHorspool();
  		String text = "poipoiboi";
  		String pattern = "boi";
  		int textstart = 5;
  		int textend = 4;
  		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
  		assertEquals(-1,location);
  		
  }
  //the following test cases are based on the functionality aspect of the length
    //of text and pattern based on the inputs to BoyerMooreHorspool implementation of StringSearch.searchString
	//this partition is about the relation among the length of Text and pattern and 32 bytes
    
    //blocks are like:
    //Text<Pattern

    //Text=Pattern
   
    //Text>Pattern
    
    //-a non null pattern
    //-a non null text
    //with TextStart, TextEnd reasonable input
    //the location returned is -1 means can find pattern in text
    
    
    //Text<Pattern
    @Test
   	public void testBoyerMooreHorspoolTextSmallerThanPatternSmallerThan32InChar(){
	    StringSearch ss = new BoyerMooreHorspool();
  		String text = "boi";
  		String pattern = "poipoiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchChars(text.toCharArray(), textstart, textend, pattern.toCharArray(), ss.processChars(pattern.toCharArray()));
  		assertEquals(-1,location);
  		}
   
    
    //Text=Pattern
    @Test
    public void testBoyerMooreHorspoolTextEqualsToPatternEqualsTo32InChar(){
    	StringSearch ss = new BoyerMooreHorspool();
  		String text = "boiboiboiboiboiboiboiboiboiboibo";
  		String pattern = "poipoiboipoipoipoiboipoipoipoibo";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(-1,location);
    }
    
    
    //Text>32>Pattern
    @Test
    public void testBoyerMooreHorspoolTextLargerThan32LargerThanPatternInChar(){
    	StringSearch ss = new BoyerMooreHorspool();
  		String text = "poiboiboiboiboiboiboiboiboiboiboiboiboiboiboi";
  		String pattern = "boiboiboiboiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(3,location);
    }
    //32>Text>Pattern
    @Test
    public void testBoyerMooreHorspool32LargerThanTextLargerThanPatternInChar(){
    	StringSearch ss = new BoyerMooreHorspool();
  		String text = "poipoiboiboiboiboi";
  		String pattern = "boiboi";
  		int textstart = 0;
  		int textend = text.length();
  		int location = ss.searchBytes(text.getBytes(), textstart, textend, pattern.getBytes(), ss.processBytes(pattern.getBytes()));
  		assertEquals(6,location);
    }

 
    
    
    
   
    
    
    
    
    
    
    
   
   
   
   
   
   
   
   
   
   
    //The following 3 test cases are based on the functionality aspect of the length
    //of text and pattern based on the inputs to BoyerMooreHorspool implementation of StringSearch.searchBytes
	//this partition is about the relation between the length of text and pattern
	//there are 3 kinds of blocks: the length of text is longer, their length are the same, the length of pattern is longer
    
	//-a non null pattern
    //-a non null text
	//the length of text is longer than that of pattern
    //the location returned is 2 means can find pattern in text and the position is 2
    @Test
    public void testBoyerMooreHorspoolLengthOfTextLongerThanPatternInBytes(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = new byte[]{1,2,3,4,5};
    	byte[] pattern = new byte[]{3,4,5};
    	int textstart = 0;
    	int textend = text.length;
    	Object aa = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, textstart, textend, pattern, aa);
    	System.out.println(location);
    	assertEquals(2, location);
    	}
    
    //-a non null pattern
    //-a non null text
	//the length of text is the same that of pattern
    //the location returned is -1 means cannot find pattern in text
    @Test
    public void testBoyerMooreHorspoolLengthOfTextequalsPatternInBytes(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = new byte[]{'a','b','c','d','e'};
    	byte[] pattern = new byte[]{'b','c','d','e',1};
    	int location = ss.searchBytes(text, pattern);
    	assertEquals(-1, location);
    }
    
    //-a non null pattern
    //-a non null text
	//the length of text is shorter than that of pattern
    //the location returned is -1 means cannot find pattern in text

    @Test
    public void testBoyerMooreHorspoolLengthOfTextShorterThanPatternWithObject(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = new byte[]{3,5};
    	byte[] pattern = new byte[]{1,2,3,4,5};
    	int location = ss.searchBytes(text, pattern);
    	assertEquals(-1, location);
    }
    
	// Technique used: input space partitioning
	//
	// The following three test cases represents the block of inputs 
	// to the BoyerMooreHorspool implementation of StringSearch.searchString
	// that each contains:
	//
	// a non-null pattern, and a non-null text, and
	// the pattern does not occur in the text or
	// the pattern occurs once in the text or
	// the pattern occurs more than once in the text
    @Test
    public void testBoyerMooreHorspoolStringZeroDuplication(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "duplicate";
    	String pattern = "no";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolStringOneDuplication(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "once a while";
    	String pattern = "once";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolStringMoreDuplications(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "two for two";
    	String pattern = "two";
    	Object processed = ss.processString(pattern);
    	int locationA = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	int locationB = ss.searchString(text, locationA, text.length() - 1, pattern, processed);
    	assertEquals(pattern.charAt(locationA), pattern.charAt(locationB));
    }
    
    // Technique used: input space partitioning
	//
	// The following two test cases represents the block of inputs 
    // to the BoyerMooreHorspool implementation of StringSearch.searchString
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern is not found in the text or
 	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testBoyerMooreHorspoolStringNoVerification(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "duplicate";
    	String pattern = "no";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolStringVerification(){
    	StringSearch ss = new BoyerMooreHorspool();
    	String text = "once a while";
    	String pattern = "once";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(text.charAt(location), pattern.charAt(0));
    }
    
	// Technique used: input space partitioning
	//
	// The following three test cases represents the block of inputs 
	// to the BoyerMooreHorspool implementation of StringSearch.searchChars
	// that each contains:
	//
	// a non-null pattern, and a non-null text, and
	// the pattern does not occur in the text or
	// the pattern occurs once in the text or
	// the pattern occurs more than once in the text
    @Test
    public void testBoyerMooreHorspoolCharsZeroDuplication(){
    	StringSearch ss = new BoyerMooreHorspool();
    	char[] text = {'d','u','p','l','i','c','a','t','e'};
    	char[] pattern = {'n','o'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolCharsOneDuplication(){
    	StringSearch ss = new BoyerMooreHorspool();
    	char[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	char[] pattern = {'o','n','c','e'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolCharsMoreDuplications(){
    	StringSearch ss = new BoyerMooreHorspool();
    	char[] text = {'t','w','o',' ','f','o','r',' ','t','w','o'};
    	char[] pattern = {'t','w','o'};
    	Object processed = ss.processChars(pattern);
    	int locationA = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	int locationB = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[locationA], text[locationB]);
    }
    
    // Technique used: input space partitioning
	//
	// The following two test cases represents the block of inputs 
    // to the BoyerMooreHorspool implementation of StringSearch.searchChars
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern is not found in the text or
 	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testBoyerMooreHorspoolCharsNoVerification(){
    	StringSearch ss = new BoyerMooreHorspool();
    	char[] text = {'d','u','p','l','i','c','a','t','e'};
    	char[] pattern = {'N','o'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolCharsVerification(){
    	StringSearch ss = new BoyerMooreHorspool();
    	char[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	char[] pattern = {'o','n','c','e'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[location], pattern[0]);
    }
    
    // Technique used: input space partitioning
 	//
 	// The following three test cases represents the block of inputs 
 	// to the BoyerMooreHorspool implementation of StringSearch.searchBytes
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern does not occur in the text or
 	// the pattern occurs once in the text or
 	// the pattern occurs more than once in the text
    @Test
    public void testBoyerMooreHorspoolBytesZeroDuplication(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = {'d','u','p','l','i','c','a','t','e'};
    	byte[] pattern = {'n','o'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolBytesOneDuplication(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	byte[] pattern = {'o','n','c','e'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolBytesMoreDuplications(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = {'t','w','o',' ','f','o','r',' ','t','w','o'};
    	byte[] pattern = {'t','w','o'};
    	Object processed = ss.processBytes(pattern);
    	int locationA = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	int locationB = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[locationA], text[locationB]);
    }
    
    // Technique used: input space partitioning
  	//
  	// The following two test cases represents the block of inputs 
    // to the BoyerMooreHorspool implementation of StringSearch.searchBytes
   	// that each contains:
   	//
   	// a non-null pattern, and a non-null text, and
   	// the pattern is not found in the text or
   	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testBoyerMooreHorspoolBytesNoVerification(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = {'d','u','p','l','i','c','a','t','e'};
    	byte[] pattern = {'N','o'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testBoyerMooreHorspoolBytesVerification(){
    	StringSearch ss = new BoyerMooreHorspool();
    	byte[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	byte[] pattern = {'o','n','c','e'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[location], pattern[0]);
    }
}

