package com.eaio.stringsearch.inputSpaceTest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.eaio.stringsearch.ShiftOrMismatches;
import com.eaio.stringsearch.StringSearch;

public class ShiftOrMismatchesTest {
    
	// Technique used: input space partitioning
	//
	// The following three test cases represents the block of inputs 
	// to the ShiftOrMismatches implementation of StringSearch.searchString
	// that each contains:
	//
	// a non-null pattern, and a non-null text, and
	// the pattern does not occur in the text or
	// the pattern occurs once in the text or
	// the pattern occurs more than once in the text
    @Test
    public void testShiftOrMismatchesStringZeroDuplication(){
    	StringSearch ss = new ShiftOrMismatches();
    	String text = "duplicate";
    	String pattern = "no";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testShiftOrMismatchesStringOneDuplication(){
    	StringSearch ss = new ShiftOrMismatches();
    	String text = "once a while";
    	String pattern = "once";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testShiftOrMismatchesStringMoreDuplications(){
    	StringSearch ss = new ShiftOrMismatches();
    	String text = "two for two";
    	String pattern = "two";
    	Object processed = ss.processString(pattern);
    	int locationA = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	int locationB = ss.searchString(text, locationA, text.length() - 1, pattern, processed);
    	assertEquals(pattern.charAt(locationA), pattern.charAt(locationB));
    }
    
    // Technique used: input space partitioning
	//
	// The following two test cases represents the block of inputs 
    // to the ShiftOrMismatches implementation of StringSearch.searchString
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern is not found in the text or
 	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testShiftOrMismatchesStringNoVerification(){
    	StringSearch ss = new ShiftOrMismatches();
    	String text = "duplicate";
    	String pattern = "no";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testShiftOrMismatchesStringVerification(){
    	StringSearch ss = new ShiftOrMismatches();
    	String text = "once a while";
    	String pattern = "once";
    	Object processed = ss.processString(pattern);
    	int location = ss.searchString(text, 0, text.length() - 1, pattern, processed);
    	assertEquals(text.charAt(location), pattern.charAt(0));
    }
    
	// Technique used: input space partitioning
	//
	// The following three test cases represents the block of inputs 
	// to the ShiftOrMismatches implementation of StringSearch.searchChars
	// that each contains:
	//
	// a non-null pattern, and a non-null text, and
	// the pattern does not occur in the text or
	// the pattern occurs once in the text or
	// the pattern occurs more than once in the text
    @Test
    public void testShiftOrMismatchesCharsZeroDuplication(){
    	StringSearch ss = new ShiftOrMismatches();
    	char[] text = {'d','u','p','l','i','c','a','t','e'};
    	char[] pattern = {'n','o'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testShiftOrMismatchesCharsOneDuplication(){
    	StringSearch ss = new ShiftOrMismatches();
    	char[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	char[] pattern = {'o','n','c','e'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testShiftOrMismatchesCharsMoreDuplications(){
    	StringSearch ss = new ShiftOrMismatches();
    	char[] text = {'t','w','o',' ','f','o','r',' ','t','w','o'};
    	char[] pattern = {'t','w','o'};
    	Object processed = ss.processChars(pattern);
    	int locationA = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	int locationB = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[locationA], text[locationB]);
    }
    
    // Technique used: input space partitioning
	//
	// The following two test cases represents the block of inputs 
    // to the ShiftOrMismatches implementation of StringSearch.searchChars
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern is not found in the text or
 	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testShiftOrMismatchesCharsNoVerification(){
    	StringSearch ss = new ShiftOrMismatches();
    	char[] text = {'d','u','p','l','i','c','a','t','e'};
    	char[] pattern = {'n','o'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testShiftOrMismatchesCharsVerification(){
    	StringSearch ss = new ShiftOrMismatches();
    	char[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	char[] pattern = {'o','n','c','e'};
    	Object processed = ss.processChars(pattern);
    	int location = ss.searchChars(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[location], pattern[0]);
    }
    
    // Technique used: input space partitioning
 	//
 	// The following three test cases represents the block of inputs 
 	// to the ShiftOrMismatches implementation of StringSearch.searchBytes
 	// that each contains:
 	//
 	// a non-null pattern, and a non-null text, and
 	// the pattern does not occur in the text or
 	// the pattern occurs once in the text or
 	// the pattern occurs more than once in the text
    @Test
    public void testShiftOrMismatchesBytesZeroDuplication(){
    	StringSearch ss = new ShiftOrMismatches();
    	byte[] text = {'d','u','p','l','i','c','a','t','e'};
    	byte[] pattern = {'n','o'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testShiftOrMismatchesBytesOneDuplication(){
    	StringSearch ss = new ShiftOrMismatches();
    	byte[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	byte[] pattern = {'o','n','c','e'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(0, location);
    }
    
    @Test
    public void testShiftOrMismatchesBytesMoreDuplications(){
    	StringSearch ss = new ShiftOrMismatches();
    	byte[] text = {'t','w','o',' ','f','o','r',' ','t','w','o'};
    	byte[] pattern = {'t','w','o'};
    	Object processed = ss.processBytes(pattern);
    	int locationA = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	int locationB = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[locationA], text[locationB]);
    }
    
    // Technique used: input space partitioning
  	//
  	// The following two test cases represents the block of inputs 
    // to the ShiftOrMismatches implementation of StringSearch.searchBytes
   	// that each contains:
   	//
   	// a non-null pattern, and a non-null text, and
   	// the pattern is not found in the text or
   	// the pattern is found in the text while
    // the found sub-text matches the pattern
    @Test
    public void testShiftOrMismatchesBytesNoVerification(){
    	StringSearch ss = new ShiftOrMismatches();
    	byte[] text = {'d','u','p','l','i','c','a','t','e'};
    	byte[] pattern = {'n','o'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(-1, location);
    }
    
    @Test
    public void testShiftOrMismatchesBytesVerification(){
    	StringSearch ss = new ShiftOrMismatches();
    	byte[] text = {'o','n','c','e',' ','a',' ','w','h','i','l','e'};
    	byte[] pattern = {'o','n','c','e'};
    	Object processed = ss.processBytes(pattern);
    	int location = ss.searchBytes(text, 0, text.length - 1, pattern, processed);
    	assertEquals(text[location], pattern[0]);
    }
}