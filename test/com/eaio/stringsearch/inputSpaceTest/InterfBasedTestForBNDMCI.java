package com.eaio.stringsearch.inputSpaceTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.eaio.stringsearch.BNDMCI;
import com.eaio.stringsearch.StringSearch;

public class InterfBasedTestForBNDMCI {
	private static int NOT_FOUND = -1;
	private StringSearch ssBNDMCI;
	private int loc;
	
	@Before
	public void init() {
		ssBNDMCI = new BNDMCI();
		loc = Integer.MIN_VALUE;
	}
	
	/* This test suite is used to perform interface-based input space test.
	 * To ignore those wrapper function, this test pick the rawest three:
	 * 		int searchBytes(byte[] text, int textStart, int textEnd,
     *                              byte[] pattern, Object processed);
     *      int searchChars(char[] text, int textStart, int textEnd,
     *                              char[] pattern, Object processed);
	 * 		int searchString(String text, int textStart, int textEnd,
     *                            String pattern, Object processed);
     *                            
     * There are five similar parameters for each function.
     * 		For "text":
     * 			Block1 (IsNull) : Null
     * 			Block2 (IsEmpty): Not null, but empty.
     * 			Block3 (HasCtnt): Neither null nor empty.
     * 		For "testStart":
     * 			Block1 (LTZero)	: (-infinity, 0) 
     * 			Block2 (IsZero)	: [0, 0]
     * 			Block3 (GTZero) : (0, +infinity)
     * 		For "textEnd":
     * 			Block1 (LTZero)	: (-infinity, 0) 
     * 			Block2 (IsZero)	: [0, 0]
     * 			Block3 (GTZero) : (0, +infinity)
     * 		For "pattern":
     * 			Block1 (IsNull) : Null
     * 			Block2 (IsEmpty): Not null, but empty.
     * 			Block3 (HasCtnt): Neither null nor empty
     * 		For "processed":
     * 			Block1 (IsNull) : Null
     * 			Block2 (NtNull) : Not Null
     * 
     * 		For all combination situation, there should be 182 test cases.
     * 		But those hundreds of test cases may have redundancies.
     * 		Thus, we would like to use "Base choice method" to cover the most
     * 		common situation with fewer cases.
     * 
     *  	The base choice is: {B3, B3, B3, B3, B2} 
     *  	(This combination is considered as the most frequent situation)
     *  
     *  	Then, the others are:
     *  	C01: {B1, B3, B3, B3, B2} C02: {B2, B3, B3, B3, B2} 
     *  	C03: {B3, B1, B3, B3, B2} C04: {B3, B2, B3, B3, B2}  
     *  	C05: {B3, B3, B1, B3, B2} C06: {B3, B3, B2, B3, B2} 
     *  	C07: {B3, B3, B3, B1, B2} C08: {B3, B3, B3, B2, B2} 
     *  	C09: {B3, B3, B3, B3, B1} 
     *  
     *  	*Notion: 
     *  		All test cases take the instruction of function 
     *  		as the requirement and are designed according to it.
	 */
	
	/* For txt and ptn are the type of String  */
    //C00: {B3, B3, B3, B3, B1}
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMCI.processString(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}

    //C01: {B1, B3, B3, B3, B1}
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtIsNull_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForStr(){
		String txt = null;
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMCI.processString(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
	}
	
	//C02: {B2, B3, B3, B3, B1} TODO: Here is a failure: OutOfBoundsException on "searchString"
	@Ignore
	public void ist_ssBNDMCI_txtIsEmpty_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForStr(){
		String txt = "";
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMCI.processString(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
    //C03: {B3, B1, B3, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSLTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = -1;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMCI.processString(ptn);
		
		assertTrue(tS < 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	//C04: {B3, B2, B3, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSIsZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMCI.processString(ptn);
		
		assertTrue(tS == 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}
	
    //C05: {B3, B3, B1, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tELTZero_ptnHasCtnt_prcdNtNull_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = -1;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMCI.processString(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE < 0);
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
	//C06: {B3, B3, B2, B3, B1}
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEIsZero_ptnHasCtnt_prcdNtNull_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 0;
		String ptn = "\t[031492] Su";
		Object pObj = ssBNDMCI.processString(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE == 0);		 
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
		
    //C07: {B3, B3, B3, B1, B1} 
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnIsNull_prcdNtNull_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = null;
		Object pObj = ssBNDMCI.processString(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
	}
	
	//C08: {B3, B3, B3, B2, B1} TODO: Here is a failure: Cannot covert empty string.
	@Ignore
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnIsEmpty_prcdNtNull_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "";
		Object pObj = ssBNDMCI.processString(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
    //C09: {B3, B3, B3, B3, B2}
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdIsNull_ForStr(){
		String txt = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String ptn = "\t[031492] Su";
		Object pObj = null;

		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn);
		assertTrue(loc == 3);
		loc = ssBNDMCI.searchString(txt, tS, tE, ptn, pObj);
	} 
	
	/*
	 *
	 */

	/* For txt and ptn are the type of byte[]  */
    //C00: {B3, B3, B3, B3, B1}
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}

    //C01: {B1, B3, B3, B3, B1}
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtIsNull_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForBytes(){
		String text = null;
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);	
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
	}
	
	//C02: {B2, B3, B3, B3, B1} TODO: Here is a failure: OutOfBoundsException on "searchBytes"
	@Ignore
	public void ist_ssBNDMCI_txtIsEmpty_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForBytes(){
		String text = "";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);	
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
    //C03: {B3, B1, B3, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSLTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = -1;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);
		
		assertTrue(tS < 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	//C04: {B3, B2, B3, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSIsZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);
		
		assertTrue(tS == 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}
	
    //C05: {B3, B3, B1, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tELTZero_ptnHasCtnt_prcdNtNull_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = -1;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE < 0);
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
	//C06: {B3, B3, B2, B3, B1}
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEIsZero_ptnHasCtnt_prcdNtNull_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 0;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE == 0);
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
    //C07: {B3, B3, B3, B1, B1} 
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnIsNull_prcdNtNull_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = null;
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
	}
	
	//C08: {B3, B3, B3, B2, B1} TODO: Here is a failure: Cannot covert empty string.
	@Ignore
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnIsEmpty_prcdNtNull_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = ssBNDMCI.processBytes(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
    //C09: {B3, B3, B3, B3, B2}
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdIsNull_ForBytes(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		byte[] txt = text.getBytes();
		byte[] ptn = pattern.getBytes();
		Object pObj = null;

		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn);
		assertTrue(loc == 3);
		loc = ssBNDMCI.searchBytes(txt, tS, tE, ptn, pObj);
	} 
	
	/*
	 *
	 */

	/* For txt and ptn are the type of char[]  */
    //C00: {B3, B3, B3, B3, B1}
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}

    //C01: {B1, B3, B3, B3, B1}
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtIsNull_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForChars(){
		String text = null;
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);		
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
	}
	
	//C02: {B2, B3, B3, B3, B1} TODO: Here is a failure: OutOfBoundsException on "searchChars"
	@Ignore
	public void ist_ssBNDMCI_txtIsEmpty_tSGTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForChars(){
		String text = "";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);		
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
    //C03: {B3, B1, B3, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSLTZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = -1;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);
		
		assertTrue(tS < 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}
	
	//C04: {B3, B2, B3, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSIsZero_tEGTZero_ptnHasCtnt_prcdNtNull_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 0;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);
		
		assertTrue(tS == 0);
		assertTrue(tE > 0);		 
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == 3);
	}
	
    //C05: {B3, B3, B1, B3, B1} 
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tELTZero_ptnHasCtnt_prcdNtNull_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = -1;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE < 0);
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
	//C06: {B3, B3, B2, B3, B1}
	@Test
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEIsZero_ptnHasCtnt_prcdNtNull_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 0;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);
		
		assertTrue(tS > 0);
		assertTrue(tE == 0);
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
	
    //C07: {B3, B3, B3, B1, B1} 
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnIsNull_prcdNtNull_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = null;
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);		
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
	}
	
	//C08: {B3, B3, B3, B2, B1} TODO: Here is a failure: Cannot covert empty string.
	@Ignore
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnIsEmpty_prcdNtNull_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = ssBNDMCI.processChars(ptn);

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
		assertTrue(loc == NOT_FOUND);
	}
		
    //C09: {B3, B3, B3, B3, B2}
	@Test(expected = NullPointerException.class)
	public void ist_ssBNDMCI_txtHasCtnt_tSGTZero_tEGTZero_ptnHasCtnt_prcdIsNull_ForChars(){
		String text = "!!!\t[031492] Su_Lin_Love!!!";
		int tS = 2;
		int tE = 24;
		String pattern = "\t[031492] Su";
		char[] txt = text.toCharArray();
		char[] ptn = pattern.toCharArray();
		Object pObj = null;

		assertTrue(tS > 0);
		assertTrue(tE > 0);
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn);
		assertTrue(loc == 3);
		loc = ssBNDMCI.searchChars(txt, tS, tE, ptn, pObj);
	} 
	
}
